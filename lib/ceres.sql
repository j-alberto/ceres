USE [master]
GO
/****** Object:  Database [CERES]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE DATABASE [CERES]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CERES', FILENAME = N'E:\Program Files\Microsoft SQL Server\MSSQL11.ITUDEVSRV\MSSQL\Data\CERES.mdf' , SIZE = 17586240KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'CERES_log', FILENAME = N'G:\Program Files\Microsoft SQL Server\MSSQL11.ITUDEVSRV\MSSQL\Data\CERES_log.ldf' , SIZE = 60736000KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CERES] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CERES].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CERES] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CERES] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CERES] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CERES] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CERES] SET ARITHABORT OFF 
GO
ALTER DATABASE [CERES] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CERES] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [CERES] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CERES] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CERES] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CERES] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CERES] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CERES] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CERES] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CERES] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CERES] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CERES] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CERES] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CERES] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CERES] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CERES] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CERES] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CERES] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CERES] SET RECOVERY FULL 
GO
ALTER DATABASE [CERES] SET  MULTI_USER 
GO
ALTER DATABASE [CERES] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CERES] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CERES] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CERES] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'CERES', N'ON'
GO
USE [CERES]
GO
/****** Object:  User [readUser]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE USER [readUser] FOR LOGIN [readUser] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [CGIARAD\NMONTES]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE USER [CGIARAD\NMONTES] FOR LOGIN [CGIARAD\NMONTES] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [CGIARAD\DCASTANON]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE USER [CGIARAD\DCASTANON] FOR LOGIN [CGIARAD\DCASTANON] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [cgiarad\cimmyt-linksrvs]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE USER [cgiarad\cimmyt-linksrvs] FOR LOGIN [cgiarad\cimmyt-linksrvs] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [readUser]
GO
ALTER ROLE [db_owner] ADD MEMBER [CGIARAD\DCASTANON]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [cgiarad\cimmyt-linksrvs]
GO
ALTER ROLE [db_datareader] ADD MEMBER [cgiarad\cimmyt-linksrvs]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [cgiarad\cimmyt-linksrvs]
GO
/****** Object:  Schema [cm]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE SCHEMA [cm]
GO
/****** Object:  Schema [fb]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE SCHEMA [fb]
GO
/****** Object:  Schema [gp]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE SCHEMA [gp]
GO
/****** Object:  Schema [mg]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE SCHEMA [mg]
GO
/****** Object:  Schema [on]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE SCHEMA [on]
GO
/****** Object:  Schema [tmp]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE SCHEMA [tmp]
GO
/****** Object:  StoredProcedure [dbo].[SP_copyEntries]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Raul Hernandez>
-- Create date: <10/24/2016>
-- Description:	<Crea una tabla temporal con las entradas, elimina aquellas que no estan
--               en la tabla de germoplasma>
-- =============================================
CREATE PROCEDURE [dbo].[SP_copyEntries]
AS
BEGIN
	
	IF OBJECT_ID('tmp_occEntry','U') IS NOT NULL 
    drop table tmp_occEntry;

	select ROW_NUMBER() over (order by a.id) as id,
		    ROW_NUMBER() over (order by a.id) as 'key',
			b.id as study_id, 
			a.entno,
			a.product_id,
			ISNULL(a.product_name,'unknown') as product_name,
			a.id as remarks,
			a.creation_timestamp,
			a.creator_id,
			a.id as notes,
			a.is_void,
			a.is_active  into tmp_occEntry
		from studyEntry_to_entry a, study_to_study b  
	where a.study_id = b.notes
		and a.product_id not in ( select a.germplasmId from (select distinct germplasmId from fb.studyEntry) a left join [gp].[germplasm] b
		on a.germplasmId = b.gid 
		where  b.gid is null);


END

GO
/****** Object:  StoredProcedure [dbo].[SP_copyProducts]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Stored Procedure dbo.SP_wgb_Get_info_by_country    Script Date: 06/15/2004 11:50:31 AM ******/
CREATE PROCEDURE [dbo].[SP_copyProducts]
AS
    DECLARE @tmp_gid integer

-- step 1
IF OBJECT_ID('tmp_gempTypeUnique','U') IS NOT NULL 
  drop table tmp_gempTypeUnique;

IF OBJECT_ID('tmp_nameGermp','U') IS NOT NULL 
  drop table tmp_nameGermp;

IF OBJECT_ID('tmp_tmpGids','U') IS NOT NULL 
  drop table tmp_tmpGids;

-- step 2
select germplasmId, 
	    IIF(sn = 1,2,IIF(sn = 2,4,IIF(sn = 3,5,IIF(sn = 4,1,IIF(sn = 5,10,IIF(sn = 6,7,IIF(sn = 7,6,IIF(sn = 8,9,0)))))))) typeId INTO tmp_gempTypeUnique
 from (select b.germplasmId, 
              min(IIF(c.id = 2,1,IIF(c.id = 4,2,IIF(c.id = 5,3,IIF(c.id = 1,4,IIF(c.id = 10,5,IIF(c.id = 7,6,IIF(c.id = 6,7,IIF(c.id = 9,8,0)))))))))  as sn
         from gp.name b, gp.nameType c
        where b.typeId = c.id  
     group by b.germplasmId) g; 

select b.id,a.germplasmId,a.typeId,b.value into tmp_nameGermp
  from tmp_gempTypeUnique a, gp.name b
 where a.germplasmId = b.germplasmId
  and a.typeId = b.typeId;

select top 1 * INTO tmp_tmpGids
 from tmp_nameGermp;

delete from tmp_tmpGids;

DELETE FROM tmp_nameGermp WHERE id in (
SELECT min(id) id
  FROM tmp_nameGermp where germplasmId in (
        select germplasmId from tmp_nameGermp
      group by germplasmId
        having count(1) > 1)
 group by germplasmId);

Declare intcursor cursor for select germplasmId 
                               from tmp_nameGermp
                           group by germplasmId
                             having count(1) > 1
Open intcursor
fetch intcursor into @tmp_gid
While @@fetch_status = 0
BEGIN
  
  insert into tmp_tmpGids
     select top 1 * from tmp_nameGermp where germplasmId = @tmp_gid;
    
  delete from tmp_nameGermp where germplasmId = @tmp_gid;

  fetch intcursor into @tmp_gid

END 
CLOSE intcursor;
deallocate intcursor;

 insert into tmp_nameGermp
   select * from tmp_tmpGids;

 drop table tmp_tmpGids;
 drop table tmp_gempTypeUnique;


GO
/****** Object:  Table [cm].[breedingMethod]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[breedingMethod](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[code] [nvarchar](4) NOT NULL,
	[cropId] [smallint] NOT NULL,
	[typeId] [smallint] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdByUserId] [smallint] NOT NULL,
 CONSTRAINT [PK_breedingMethod] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_breedingMethod_cropId_code] UNIQUE NONCLUSTERED 
(
	[cropId] ASC,
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [cm].[breedingMethodType]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[breedingMethodType](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[code] [nvarchar](4) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdByUserId] [smallint] NOT NULL,
 CONSTRAINT [PK_breedingMethodType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [cm].[crop]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[crop](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](4) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[statusId] [smallint] NOT NULL,
 CONSTRAINT [PK_crop] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [cm].[customField]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cm].[customField](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[entity] [nvarchar](25) NOT NULL,
	[field] [nvarchar](25) NOT NULL,
	[dataType] [char](1) NOT NULL,
	[statusId] [smallint] NOT NULL,
 CONSTRAINT [PK_customField] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cm].[georeference]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[georeference](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](8) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[hierarchy] [smallint] NOT NULL,
	[latitude] [float] NULL,
	[longitude] [float] NULL,
	[elevation] [float] NULL,
	[cropId] [smallint] NOT NULL,
	[typeId] [smallint] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[parentGeoreferenceId] [int] NULL,
	[createdOn] [datetime] NOT NULL,
	[createdByUserId] [smallint] NOT NULL,
 CONSTRAINT [PK_georeference] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [cm].[georeferenceDetail]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[georeferenceDetail](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[value] [nvarchar](50) NOT NULL,
	[georeferenceId] [int] NOT NULL,
	[customFieldId] [smallint] NOT NULL,
 CONSTRAINT [PK_georeferenceDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [cm].[georeferenceType]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[georeferenceType](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdByUserId] [smallint] NOT NULL,
 CONSTRAINT [PK_georeferenceType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [cm].[megaEnvironment]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[megaEnvironment](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[acronym] [nvarchar](15) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[cropId] [smallint] NOT NULL,
 CONSTRAINT [PK_megaEnvironment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [cm].[organization]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[organization](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[code] [nvarchar](4) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdByUserId] [smallint] NOT NULL,
 CONSTRAINT [PK_organization] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [cm].[program]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[program](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[acronym] [nvarchar](15) NOT NULL,
	[description] [nvarchar](100) NULL,
	[externalReference] [int] NULL,
	[cropId] [smallint] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdByUserId] [smallint] NOT NULL,
 CONSTRAINT [PK_program] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [cm].[season]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[season](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[code] [nvarchar](8) NOT NULL,
	[starting] [date] NOT NULL,
	[ending] [date] NULL,
	[cropId] [smallint] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdByUserId] [smallint] NOT NULL,
 CONSTRAINT [PK_season] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [cm].[status]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cm].[status](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_status] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tmp_nameGermp]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmp_nameGermp](
	[id] [bigint] NOT NULL,
	[germplasmId] [bigint] NOT NULL,
	[typeId] [int] NOT NULL,
	[value] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tmp_occEntry]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp_occEntry](
	[id] [bigint] NULL,
	[key] [bigint] NULL,
	[study_id] [int] NOT NULL,
	[entno] [int] NOT NULL,
	[product_id] [bigint] NOT NULL,
	[product_name] [nvarchar](max) NOT NULL,
	[remarks] [bigint] NOT NULL,
	[creation_timestamp] [datetime] NOT NULL,
	[creator_id] [smallint] NOT NULL,
	[notes] [bigint] NOT NULL,
	[is_void] [varchar](5) NOT NULL,
	[is_active] [varchar](4) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [fb].[class]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[class](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[acronym] [nvarchar](15) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
	[cropId] [smallint] NOT NULL,
 CONSTRAINT [PK_studyClass] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [fb].[cross]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[cross](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[successful] [bit] NOT NULL,
	[studyId] [int] NOT NULL,
	[germplasmId] [bigint] NOT NULL,
	[statusId] [smallint] NOT NULL,
 CONSTRAINT [PK_cross] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [fb].[designType]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[designType](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[cropId] [smallint] NOT NULL,
	[statusId] [smallint] NOT NULL,
 CONSTRAINT [PK_designType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [fb].[experiment]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[experiment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[studyId] [int] NOT NULL,
	[number] [smallint] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[abbreviation] [nvarchar](15) NOT NULL,
	[plots] [int] NOT NULL,
	[plotOffset] [int] NOT NULL,
	[replicates] [smallint] NOT NULL,
	[designTypeId] [smallint] NOT NULL,
	[seasonId] [smallint] NOT NULL,
	[georeferenceId] [int] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[classId] [smallint] NOT NULL,
	[megaEnvironmentId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_experiment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [fb].[experimentDetail]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[experimentDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[value] [nvarchar](50) NOT NULL,
	[experimentId] [int] NOT NULL,
	[customFieldId] [smallint] NOT NULL,
 CONSTRAINT [PK_experimentDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [fb].[observation]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[observation](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[variableId] [int] NOT NULL,
	[value] [nvarchar](255) NOT NULL,
	[valueStandard] [nvarchar](255) NULL,
	[createdOn] [datetime] NULL,
	[createdBy] [smallint] NULL,
 CONSTRAINT [PK_observation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [fb].[plot]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[plot](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[studyEntryId] [bigint] NOT NULL,
	[experimentId] [int] NOT NULL,
	[number] [int] NOT NULL,
	[replicate] [smallint] NOT NULL,
	[subBlock] [smallint] NULL,
	[row] [smallint] NULL,
	[column] [smallint] NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_plot] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [fb].[study]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[study](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[abbreviation] [nvarchar](15) NOT NULL,
	[description] [nvarchar](200) NULL,
	[starting] [date] NULL,
	[ending] [date] NULL,
	[tests] [int] NOT NULL,
	[checks] [int] NOT NULL,
	[totalEntries] [int] NOT NULL,
	[checkOffset] [int] NOT NULL,
	[checkFrequency] [int] NOT NULL,
	[checkInsertions] [smallint] NOT NULL,
	[experiments] [smallint] NOT NULL,
	[programId] [smallint] NOT NULL,
	[typeId] [smallint] NOT NULL,
	[ownerUserId] [smallint] NULL,
	[cropId] [smallint] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
	[organizationId] [smallint] NOT NULL,
	[targetMegaEnvironmentId] [smallint] NOT NULL,
	[targetSeasonId] [smallint] NOT NULL,
	[targetClassId] [smallint] NOT NULL,
 CONSTRAINT [PK_study] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [fb].[studyEntry]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[studyEntry](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[sequence] [int] NOT NULL,
	[studyId] [int] NOT NULL,
	[germplasmId] [bigint] NOT NULL,
	[typeId] [smallint] NOT NULL,
	[sourceEntryId] [bigint] NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_studyEntry] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [fb].[studyEntryType]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[studyEntryType](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[statusId] [smallint] NOT NULL,
 CONSTRAINT [PK_studyEntryType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [fb].[studyType]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [fb].[studyType](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[statusId] [smallint] NOT NULL,
 CONSTRAINT [PK_studyType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [gp].[genealogy]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [gp].[genealogy](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[germplasmId] [bigint] NOT NULL,
	[progenitorId] [bigint] NOT NULL,
	[progenitorTypeId] [smallint] NOT NULL,
 CONSTRAINT [PK_genealogy] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [gp].[germplasm]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [gp].[germplasm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[gid] [bigint] NOT NULL,
	[typeId] [smallint] NOT NULL,
	[cropId] [smallint] NOT NULL,
	[breedingMethodId] [smallint] NOT NULL,
	[georeferenceId] [int] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[speciesId] [bigint] NULL,
	[programId] [smallint] NOT NULL,
	[organizationId] [smallint] NOT NULL,
	[megaEnvironmentId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_germplasm] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [gp].[germplasmDetail]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [gp].[germplasmDetail](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[value] [nvarchar](50) NOT NULL,
	[germplasmId] [bigint] NOT NULL,
	[customFieldId] [smallint] NOT NULL,
 CONSTRAINT [PK_germplasmDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [gp].[germplasmType]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [gp].[germplasmType](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[statusId] [smallint] NOT NULL,
 CONSTRAINT [PK_germplasmType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [gp].[name]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [gp].[name](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[value] [nvarchar](max) NOT NULL,
	[abbreviation] [nvarchar](6) NULL,
	[preferred] [bit] NOT NULL,
	[typeId] [smallint] NOT NULL,
	[germplasmId] [bigint] NOT NULL,
	[georeferenceId] [int] NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_name] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [gp].[nameType]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [gp].[nameType](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[code] [nvarchar](8) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[cropId] [smallint] NOT NULL,
 CONSTRAINT [PK_nameType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [gp].[progenitorType]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [gp].[progenitorType](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](25) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[statusId] [smallint] NOT NULL,
 CONSTRAINT [PK_progenitorType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [mg].[function]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [mg].[function](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_function] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_function_name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [mg].[grant]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [mg].[grant](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[functionId] [smallint] NOT NULL,
	[create] [bit] NOT NULL,
	[read] [bit] NOT NULL,
	[update] [bit] NOT NULL,
	[delete] [bit] NOT NULL,
	[sysAdmin] [bit] NOT NULL,
	[roleId] [smallint] NOT NULL,
	[programId] [smallint] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_grant] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [mg].[person]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [mg].[person](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[firstName] [nvarchar](50) NOT NULL,
	[lastName] [nvarchar](50) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[description] [nvarchar](200) NULL,
	[titleId] [smallint] NOT NULL,
	[userId] [smallint] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_person] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_person_userId] UNIQUE NONCLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [mg].[role]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [mg].[role](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](200) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_role_name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [mg].[title]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [mg].[title](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](10) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_title] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_title_code] UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [mg].[user]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [mg].[user](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](100) NOT NULL,
	[description] [nvarchar](200) NULL,
	[externalId] [int] NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
	[passwordExpiration] [datetime] NOT NULL,
 CONSTRAINT [PK_status] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UC_user_name] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [mg].[userByRole]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [mg].[userByRole](
	[userId] [smallint] NOT NULL,
	[roleId] [smallint] NOT NULL,
 CONSTRAINT [PK_userByRole] PRIMARY KEY CLUSTERED 
(
	[userId] ASC,
	[roleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [on].[method]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [on].[method](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[abbreviation] [nvarchar](8) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[cropOntologyCode] [nchar](14) NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_method] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [on].[scale]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [on].[scale](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[abbreviation] [nvarchar](8) NOT NULL,
	[description] [nvarchar](100) NOT NULL,
	[statusId] [smallint] NOT NULL,
	[intervalType] [nchar](1) NULL,
	[dataType] [nvarchar](15) NOT NULL,
	[cropOntologyCode] [nchar](14) NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_scale] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [on].[trait]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [on].[trait](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[abbreviation] [nvarchar](8) NOT NULL,
	[description] [nvarchar](100) NULL,
	[statusId] [smallint] NOT NULL,
	[cropId] [smallint] NOT NULL,
	[cropOntologyCode] [nchar](14) NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_trait] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [on].[variable]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [on].[variable](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[traitId] [int] NOT NULL,
	[scaleId] [int] NOT NULL,
	[methodId] [int] NOT NULL,
	[cropOntologyCode] [nchar](14) NULL,
	[statusId] [smallint] NOT NULL,
	[cropId] [smallint] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[synonym] [nvarchar](50) NULL,
	[contextId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
 CONSTRAINT [PK_variable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [on].[variableContext]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [on].[variableContext](
	[id] [smallint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[cropId] [smallint] NOT NULL,
	[statusId] [smallint] NOT NULL,
 CONSTRAINT [PK_variableContext] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [tmp].[experiment]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tmp].[experiment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[studyId] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[abbreviation] [nvarchar](15) NOT NULL,
	[plots] [int] NOT NULL,
	[plotOffset] [int] NOT NULL,
	[replicates] [smallint] NOT NULL,
	[designTypeId] [smallint] NOT NULL,
	[seasonId] [smallint] NOT NULL,
	[georeferenceId] [int] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[classId] [smallint] NOT NULL,
	[megaEnvironmentId] [smallint] NOT NULL,
	[occ] [int] NULL,
 CONSTRAINT [PK_experiment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [tmp].[experimentDetail]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tmp].[experimentDetail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[value] [nvarchar](50) NOT NULL,
	[experimentId] [int] NOT NULL,
	[customFieldId] [smallint] NOT NULL,
 CONSTRAINT [PK_experimentDetail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [tmp].[germplasm]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tmp].[germplasm](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[gid] [bigint] NOT NULL,
	[typeId] [smallint] NOT NULL,
	[cropId] [smallint] NOT NULL,
	[breedingMethodId] [smallint] NOT NULL,
	[georeferenceId] [int] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[speciesId] [bigint] NULL,
	[programId] [smallint] NULL,
	[organizationId] [smallint] NULL,
	[megaEnvironmentId] [smallint] NULL,
 CONSTRAINT [PK_germplasm] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [tmp].[mappedTraits]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tmp].[mappedTraits](
	[trait] [int] NOT NULL,
	[unit] [int] NOT NULL,
	[mvariable] [nchar](14) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [tmp].[ontologyMappings]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tmp].[ontologyMappings](
	[trait] [int] NOT NULL,
	[unit] [int] NOT NULL,
	[dtype] [nvarchar](3) NULL,
	[cropOntologyId] [nvarchar](14) NULL,
	[standard] [bit] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [tmp].[plot]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tmp].[plot](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[studyEntryId] [bigint] NOT NULL,
	[experimentId] [int] NOT NULL,
	[occ] [int] NOT NULL,
	[number] [int] NOT NULL,
	[replicate] [smallint] NOT NULL,
	[subBlock] [smallint] NULL,
	[row] [smallint] NULL,
	[column] [smallint] NULL,
	[ent] [int] NOT NULL,
 CONSTRAINT [PK_plot] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [tmp].[study]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tmp].[study](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[abbreviation] [nvarchar](15) NOT NULL,
	[description] [nvarchar](200) NULL,
	[starting] [date] NULL,
	[ending] [date] NULL,
	[tests] [int] NOT NULL,
	[checks] [int] NOT NULL,
	[totalEntries] [int] NOT NULL,
	[checkOffset] [int] NOT NULL,
	[checkFrequency] [int] NOT NULL,
	[checkInsertions] [smallint] NOT NULL,
	[experiments] [smallint] NOT NULL,
	[programId] [smallint] NOT NULL,
	[typeId] [smallint] NOT NULL,
	[ownerUserId] [smallint] NULL,
	[cropId] [smallint] NOT NULL,
	[statusId] [smallint] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [smallint] NOT NULL,
	[organizationId] [smallint] NOT NULL,
	[targetMegaEnvironmentId] [smallint] NOT NULL,
	[targetSeasonId] [smallint] NOT NULL,
 CONSTRAINT [PK_study] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [tmp].[studyEntry]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tmp].[studyEntry](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[sequence] [int] NOT NULL,
	[studyId] [int] NOT NULL,
	[germplasmId] [bigint] NOT NULL,
	[typeId] [smallint] NOT NULL,
	[sourceEntryId] [bigint] NULL,
	[sourceEnt] [int] NOT NULL,
	[sourceTid] [int] NOT NULL,
	[sourceOcc] [int] NOT NULL,
 CONSTRAINT [PK_studyEntry] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [tmp].[studyTrait]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tmp].[studyTrait](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[studyId] [int] NOT NULL,
	[traitId] [smallint] NOT NULL,
	[experimentId] [int] NOT NULL,
	[unitId] [smallint] NOT NULL,
 CONSTRAINT [PK_studyTrait] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[breedingMethod_to_crossMethod]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[breedingMethod_to_crossMethod] as 
select a.id   as id,
       a.code as abbrev,
	   a.name as name,
	   a.description as description,
	   concat(a.code,'-',a.name) as display_name,
	   a.createdOn       as creation_timestamp,
	   a.createdByUserId as creatorId,
       IIF (b.name='deleted','TRUE','FALSE') as is_void   	    
 from [cm].[breedingMethod] a, cm.status b
 where a.statusId = b.id


GO
/****** Object:  View [dbo].[breedinMethod_to_gmsMethod]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[breedinMethod_to_gmsMethod] as 
select a.id          as mid, 
       b.code        as mtype, 
       'S'           as mgrp, 
	   a.code        as mcode, 
	   a.name        as mname, 
       a.description as mdesc , 
	   0             as mref, 
	   0             as mprgn, 
	   0             as mfprg, 
	   0             as mattr, 
	   0             as geneq, 
	   1             as muid, 
	   0             as lmid, 
	   20161108      as mdate 
   from cm.breedingMethod a, cm.breedingMethodType b
where a.typeId = b.id
GO
/****** Object:  View [dbo].[class_to_phase]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[class_to_phase] as 
  select a.id+101        as id,
		acronym         as abbrev,
        a.name          as name,
		a.description   as description,
		concat(a.acronym,'-',a.name)  as display_name,
		IIF (b.name='deleted','TRUE','FALSE') as is_void,
		createdOn       as creation_timestamp,
		createdBy       as creator_id
   from CERES.fb.Class a, 
        CERES.cm.status b
  where a.statusId = b.id
GO
/****** Object:  View [dbo].[georef_to_place]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create view [dbo].[georef_to_place] as 
  select a.id+10000                   as id,
         concat(a.id+10000,'-',b.name) as abbrev,
         b.name                    as name,
		 b.description             as description,
		 concat(a.name,'-',a.name) as display_name,
		 a.createdOn       as creation_timestamp,
	     a.createdByUserId as creator_id,   
		 IIF (c.name='deleted','TRUE','FALSE') as is_void,
		 a.id+10000  as geolocation_id 
   from [cm].[georeference] a, [cm].[georeferenceType] b, [cm].[status] c
  where a.typeId = b.id
    and a.statusId = c.id
GO
/****** Object:  View [dbo].[georeference_to_country]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[georeference_to_country] as 
select a.id+10000              as id,
       code              as iso_code, 
	   c.value           as hasc_code,
	   a.name            as name,
	   a.createdOn       as creation_timestamp,
	   a.createdByUserId as creatorId,
       IIF (d.name='deleted','TRUE','FALSE') as is_void   
  from [cm].[georeference] a, [cm].[georeferenceType] b, [cm].[georeferenceDetail] c,
       cm.status d 
 where a.typeId = b.id
  and a.id = c.georeferenceId
  and c.customFieldId = 4
  and b.id = 2
  and a.statusId = d.id
GO
/****** Object:  View [dbo].[georeference_to_georeference]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[georeference_to_georeference] as 
select a.id+10000        as id,
       b.id+10000        as country_id,
	   ISNULL (a.latitude,0)  as latitude,
	   ISNULL (a.longitude,0) as longitude,
	   ISNULL (a.elevation,0) as elevation,
	   b.createdOn       as creation_timestamp,
	   b.createdByUserId as creator_id,
       IIF (b.name='deleted','TRUE','FALSE') as is_void,
	   'TRUE' as is_active
  from [cm].[georeference] a, [cm].[georeference] b,
       [cm].[status] c
 where a.typeId = 3
   and b.typeId = 2
   and a.parentGeoreferenceId = b.id
   and a.statusId = c.id  

GO
/****** Object:  View [dbo].[germplasm_to_product]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  create view [dbo].[germplasm_to_product] as 
  SELECT x.gid        as id,
	     ISNULL(y.value,'unknown')     as designation,
		 ISNULL(y.name_type,'unknown') as name_type,
		 'fixed_line' as product_type,
		 ISNULL(y.value,'unknown')     as parentage,
		 'UNKNOWN'    as generation,
		 createdOn    as creation_timestamp,
		 createdBy    as creator_id,
		 'FALSE'      as is_void
    FROM [gp].[germplasm] x left join (SELECT a.id,a.germplasmId,a.typeId,a.value, 
											  CASE b.name
												when 'Intrid'            THEN 'accession_id_in_other_genebank'
												when 'Specific Name'     THEN 'common_name'
												when 'Name Abbreviation' THEN 'breeding_line_name'
												when 'Pedigree'          THEN 'cross_name'
												when 'Selection History' THEN 'derivative_name'
												when 'Non Standard Selection History' THEN 'alternative_cross_name'
												when 'Synonym Name'                   THEN 'alternative_derivative_name'
												when 'Synonym Abbreviation'  THEN 'alternative_abbreviation' 
												when 'BCID'                 THEN 'gqnpc_unique_id'
												else 'unknown' 
											  END as name_type
										FROM tmp_nameGermp a, 
											   gp.nameType b
									   WHERE a.typeId = b.id) y
    on x.gid = y.germplasmId
	where y.value is null
GO
/****** Object:  View [dbo].[method_to_method]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create view [dbo].[method_to_method] as 
  select id               as id,
         abbreviation     as abbrev,
		 name,
		 description,
		 concat(id,'-',name) as display_name,
		 createdOn           as creation_timestamp,
		 createdBy           as creator_id,
		 cropOntologyCode    as ontology_id  
    from [on].[method]
GO
/****** Object:  View [dbo].[name_to_name]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[name_to_name] as 
select id             as nid,
       germplasmId    as gid,
	   typeId         as ntype,
	   cast(preferred as int)      as nstat,
	   0              as nuid, 
       value          as nval,
	   isnull(georeferenceId,561) as nlocn,
	   createdOn      as ndate,
	   0              as nref,
	   abbreviation   as notes   
  from gp.name
where LEN(value) = 255
and id > 4448862
GO
/****** Object:  View [dbo].[organization_to_organization]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[organization_to_organization] as
select a.id   as id,
       a.code as abbrev,
	   a.name as name,
	   concat(a.code,'-',a.name) as display_name,
	   createdOn       as creation_timestamp,
	   createdByUserId as creator_id,
	   IIF (b.name='deleted','TRUE','FALSE') as is_void   
  from [cm].[organization] a, cm.status b
where a.statusId = b.id


GO
/****** Object:  View [dbo].[plot_to_plot]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  create view [dbo].[plot_to_plot] as 
		  select a.id             as id, 
		         a.id             as 'key', 
				 a.experimentId   as study_id,
		         b.id             as entry_id,
				 abs(a.replicate) as rep,
				 a.number         as plotno,
		         a.createdOn      as creation_timestamp,
		         a.createdBy      as creator_id,
		         'FALSE'       as is_void
		    from [fb].[plot] a, tmp_occEntry b
		   where a.studyEntryId = b.notes 
		     and a.experimentId = b.study_id
GO
/****** Object:  View [dbo].[program_to_program]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[program_to_program] as 
select a.id+101          as id,
       a.acronym     as abbrev,
	   a.name        as name,
	   a.description as description,
	   concat(a.acronym,'-',a.description)  as display_name,
	   a.createdOn       as creation_timestamp,
	   a.createdByUserId as creator_id,
	   IIF (b.name='deleted','TRUE','FALSE') as is_void
  from ceres.cm.program a, 
       CERES.cm.status b
 where a.statusId = b.id
GO
/****** Object:  View [dbo].[role_to_role]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[role_to_role] as 
	select a.id as id,
       upper(a.name) as abbrev,
	   a.name as name,
	   a.description as description,
	   a.name as display_name,
	   1 as rank, 
	   a.createdOn as creation_timestamp,
	   a.createdBy as creator_id,
	   IIF (b.name='deleted','TRUE','FALSE') as is_void 
  from [mg].[role] a, [cm].[status] b
  where a.statusId = b.id
GO
/****** Object:  View [dbo].[scale_to_scale]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[scale_to_scale] as 
		  select id           as id,
		         abbreviation as abbrev,
				 name         as name,
				 dataType     as type,
				 description  as description,
				 concat(id,' - ',name) as display_name,
				 createdOn    as creation_timestamp,
				 createdBy    as creator_id,
				 'FALSE'      as is_void,
				 cropOntologyCode as ontology_id
		    from [on].[scale]
GO
/****** Object:  View [dbo].[season_to_season]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[season_to_season] as
select a.id   as id,
       a.code as abbrev,
	   a.name as name,
	   a.name as display_name,
	   concat(a.starting,' - ',a.ending) as remarks,
	   a.createdOn       as creation_timestamp,
	   a.createdByUserId as creator_id,
	   IIF (b.name='deleted','TRUE','FALSE') as is_void  
  from cm.season a, cm.status b
where a.statusId = b.id
and a.cropId = 2


GO
/****** Object:  View [dbo].[study_to_study]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[study_to_study] as 
select b.id              as id, 
       b.id              as 'key',
	   a.programId+101   as program_id,
	   b.georeferenceId+10000 as place_id,
	   b.classId+100          as phase_id,
	   year(a.createdOn) as year,
	   b.seasonId        as season_id,
	   b.id              as number,
	   concat(b.id,'-',b.name)  as name,
	   b.abbreviation    as title,
	   a.description     as remarks,
	   b.createdOn       as creation_timestamp,
	   b.createdBy       as creator_id,
	   a.id              as notes,
	   IIF (b.name='deleted','TRUE','FALSE') as is_void,
	   'TRUE'  as is_active,
	   'FALSE' as is_draft,
	   c.name  as study_status 
  from [fb].[study] a, [fb].[experiment] b, [cm].[status] c
where a.id = b.studyId
  and b.statusId = c.id 
GO
/****** Object:  View [dbo].[studyEntry_to_entry]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	create view [dbo].[studyEntry_to_entry] as 
	select a.id as id,
	       concat(a.studyId,'',a.sequence) as 'key',
		   a.studyId     as study_id,
		   a.sequence    as entno,
		   a.germplasmId as product_id,
		   b.value       as product_name,
		   a.createdOn   as creation_timestamp,
		   a.createdBy   as creator_id,
		   'FALSE'       as is_void,
		   'TRUE'        as is_active
	  from fb.studyEntry a left join tmp_nameGermp b
	    on a.germplasmId = b.germplasmId
GO
/****** Object:  View [dbo].[userperson_to_user]    Script Date: 11/29/2016 4:04:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[userperson_to_user] as 
  select a.id as id,
       b.email as email, 
       a.name as username,
	   d.id as user_type,
	   IIF (e.name='active','1','0') as status,
	   b.lastname as last_name,
	   b.firstName as first_name,
	   a.name as display_name,
	   a.createdOn as creation_timestamp,
	   a.createdBy as creator_id,
	   IIF (e.name='deleted','TRUE','FALSE') as is_void  
  from [mg].[user] a, 
       [mg].[person] b,
	   [mg].[userByRole] c,
	   [mg].[role] d,
	   [cm].[status] e
where a.id = b.userId
  and a.id = c.userId
  and d.id = c.roleId
  and a.statusId = e.id
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_breedingMethodType_code]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_breedingMethodType_code] ON [cm].[breedingMethodType]
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_crop_code]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_crop_code] ON [cm].[crop]
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_customField_entity_field]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_customField_entity_field] ON [cm].[customField]
(
	[entity] ASC,
	[field] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_georeference_code_typeId_parentGeoreferenceId]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_georeference_code_typeId_parentGeoreferenceId] ON [cm].[georeference]
(
	[code] ASC,
	[typeId] ASC,
	[parentGeoreferenceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_georeferenceType_name]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_georeferenceType_name] ON [cm].[georeferenceType]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_program_acronym]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_program_acronym] ON [cm].[program]
(
	[acronym] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_program_name]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_program_name] ON [cm].[program]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_season_cropId_code]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_season_cropId_code] ON [cm].[season]
(
	[cropId] ASC,
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_disegnType_name]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_disegnType_name] ON [fb].[designType]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_study_cropId_name]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE NONCLUSTERED INDEX [IX_study_cropId_name] ON [fb].[study]
(
	[cropId] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_studyEntry_studyId_germplasmId]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE NONCLUSTERED INDEX [IX_studyEntry_studyId_germplasmId] ON [fb].[studyEntry]
(
	[studyId] ASC,
	[germplasmId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_studyEntryType_name]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_studyEntryType_name] ON [fb].[studyEntryType]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_studyType_name]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_studyType_name] ON [fb].[studyType]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_germplasm_cropId_gid]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_germplasm_cropId_gid] ON [gp].[germplasm]
(
	[cropId] ASC,
	[gid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_germplasmType_name]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_germplasmType_name] ON [gp].[germplasmType]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_nameType_code]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_nameType_code] ON [gp].[nameType]
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_progenitorType_name]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_progenitorType_name] ON [gp].[progenitorType]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_method_cropOntologyCode]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_method_cropOntologyCode] ON [on].[method]
(
	[cropOntologyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_scale_cropOntologyCode]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_scale_cropOntologyCode] ON [on].[scale]
(
	[cropOntologyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_trait_cropOntologyCode]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_trait_cropOntologyCode] ON [on].[trait]
(
	[cropOntologyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_variable_cropOntologyCode]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_variable_cropOntologyCode] ON [on].[variable]
(
	[cropOntologyCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_studyEntry_sourceTid_sourceEnt]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE NONCLUSTERED INDEX [IX_studyEntry_sourceTid_sourceEnt] ON [tmp].[studyEntry]
(
	[sourceTid] ASC,
	[sourceEnt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_studyEntry_studyId_sequence]    Script Date: 11/29/2016 4:04:09 PM ******/
CREATE NONCLUSTERED INDEX [IX_studyEntry_studyId_sequence] ON [tmp].[studyEntry]
(
	[studyId] ASC,
	[sequence] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [cm].[customField] ADD  CONSTRAINT [DF_customField_dataType]  DEFAULT ('C') FOR [dataType]
GO
ALTER TABLE [fb].[cross] ADD  CONSTRAINT [DF_cross_successful]  DEFAULT ((0)) FOR [successful]
GO
ALTER TABLE [fb].[experiment] ADD  DEFAULT ((0)) FOR [plotOffset]
GO
ALTER TABLE [fb].[experiment] ADD  DEFAULT ((1)) FOR [replicates]
GO
ALTER TABLE [fb].[experiment] ADD  DEFAULT ((0)) FOR [megaEnvironmentId]
GO
ALTER TABLE [fb].[study] ADD  CONSTRAINT [DF_study_checks]  DEFAULT ((0)) FOR [checks]
GO
ALTER TABLE [fb].[study] ADD  CONSTRAINT [DF_study_checkOffset]  DEFAULT ((0)) FOR [checkOffset]
GO
ALTER TABLE [fb].[study] ADD  CONSTRAINT [DF_study_checkFrequency]  DEFAULT ((0)) FOR [checkFrequency]
GO
ALTER TABLE [fb].[study] ADD  CONSTRAINT [DF_study_checkInsertions]  DEFAULT ((1)) FOR [checkInsertions]
GO
ALTER TABLE [fb].[study] ADD  CONSTRAINT [DF_study_experiments]  DEFAULT ((1)) FOR [experiments]
GO
ALTER TABLE [fb].[study] ADD  CONSTRAINT [DF_study_targetMegaEnvironmentId]  DEFAULT ((0)) FOR [targetMegaEnvironmentId]
GO
ALTER TABLE [gp].[germplasm] ADD  CONSTRAINT [DF_germplasm_programId]  DEFAULT ((0)) FOR [programId]
GO
ALTER TABLE [gp].[germplasm] ADD  CONSTRAINT [DF_germplasm_organizationId]  DEFAULT ((0)) FOR [organizationId]
GO
ALTER TABLE [gp].[name] ADD  CONSTRAINT [DF_name_preferred]  DEFAULT ((0)) FOR [preferred]
GO
ALTER TABLE [cm].[breedingMethod]  WITH CHECK ADD  CONSTRAINT [FK_breedingMethod_breedingMethodType] FOREIGN KEY([typeId])
REFERENCES [cm].[breedingMethodType] ([id])
GO
ALTER TABLE [cm].[breedingMethod] CHECK CONSTRAINT [FK_breedingMethod_breedingMethodType]
GO
ALTER TABLE [cm].[breedingMethod]  WITH CHECK ADD  CONSTRAINT [FK_breedingMethod_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [cm].[breedingMethod] CHECK CONSTRAINT [FK_breedingMethod_crop]
GO
ALTER TABLE [cm].[breedingMethod]  WITH CHECK ADD  CONSTRAINT [FK_breedingMethod_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [cm].[breedingMethod] CHECK CONSTRAINT [FK_breedingMethod_status]
GO
ALTER TABLE [cm].[breedingMethodType]  WITH CHECK ADD  CONSTRAINT [FK_breedingMethodType_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [cm].[breedingMethodType] CHECK CONSTRAINT [FK_breedingMethodType_status]
GO
ALTER TABLE [cm].[crop]  WITH CHECK ADD  CONSTRAINT [FK_crop_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [cm].[crop] CHECK CONSTRAINT [FK_crop_status]
GO
ALTER TABLE [cm].[customField]  WITH CHECK ADD  CONSTRAINT [FK_customField_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [cm].[customField] CHECK CONSTRAINT [FK_customField_status]
GO
ALTER TABLE [cm].[georeference]  WITH CHECK ADD  CONSTRAINT [FK_georeference_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [cm].[georeference] CHECK CONSTRAINT [FK_georeference_crop]
GO
ALTER TABLE [cm].[georeference]  WITH CHECK ADD  CONSTRAINT [FK_georeference_georeference] FOREIGN KEY([parentGeoreferenceId])
REFERENCES [cm].[georeference] ([id])
GO
ALTER TABLE [cm].[georeference] CHECK CONSTRAINT [FK_georeference_georeference]
GO
ALTER TABLE [cm].[georeference]  WITH CHECK ADD  CONSTRAINT [FK_georeference_georeferenceType] FOREIGN KEY([typeId])
REFERENCES [cm].[georeferenceType] ([id])
GO
ALTER TABLE [cm].[georeference] CHECK CONSTRAINT [FK_georeference_georeferenceType]
GO
ALTER TABLE [cm].[georeference]  WITH CHECK ADD  CONSTRAINT [FK_georeference_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [cm].[georeference] CHECK CONSTRAINT [FK_georeference_status]
GO
ALTER TABLE [cm].[georeferenceDetail]  WITH CHECK ADD  CONSTRAINT [FK_georeferenceDetail_customField] FOREIGN KEY([customFieldId])
REFERENCES [cm].[customField] ([id])
GO
ALTER TABLE [cm].[georeferenceDetail] CHECK CONSTRAINT [FK_georeferenceDetail_customField]
GO
ALTER TABLE [cm].[georeferenceDetail]  WITH CHECK ADD  CONSTRAINT [FK_georeferenceDetail_georeference] FOREIGN KEY([georeferenceId])
REFERENCES [cm].[georeference] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [cm].[georeferenceDetail] CHECK CONSTRAINT [FK_georeferenceDetail_georeference]
GO
ALTER TABLE [cm].[georeferenceType]  WITH CHECK ADD  CONSTRAINT [FK_georeferenceType_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [cm].[georeferenceType] CHECK CONSTRAINT [FK_georeferenceType_status]
GO
ALTER TABLE [cm].[megaEnvironment]  WITH CHECK ADD  CONSTRAINT [FK_megaEnvironment_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [cm].[megaEnvironment] CHECK CONSTRAINT [FK_megaEnvironment_crop]
GO
ALTER TABLE [cm].[megaEnvironment]  WITH CHECK ADD  CONSTRAINT [FK_megaEnvironment_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [cm].[megaEnvironment] CHECK CONSTRAINT [FK_megaEnvironment_status]
GO
ALTER TABLE [cm].[organization]  WITH CHECK ADD  CONSTRAINT [FK_organization_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [cm].[organization] CHECK CONSTRAINT [FK_organization_status]
GO
ALTER TABLE [cm].[program]  WITH CHECK ADD  CONSTRAINT [FK_program_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [cm].[program] CHECK CONSTRAINT [FK_program_crop]
GO
ALTER TABLE [cm].[program]  WITH CHECK ADD  CONSTRAINT [FK_program_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [cm].[program] CHECK CONSTRAINT [FK_program_status]
GO
ALTER TABLE [cm].[season]  WITH CHECK ADD  CONSTRAINT [FK_season_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [cm].[season] CHECK CONSTRAINT [FK_season_crop]
GO
ALTER TABLE [cm].[season]  WITH CHECK ADD  CONSTRAINT [FK_season_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [cm].[season] CHECK CONSTRAINT [FK_season_status]
GO
ALTER TABLE [fb].[class]  WITH CHECK ADD  CONSTRAINT [FK_class_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [fb].[class] CHECK CONSTRAINT [FK_class_crop]
GO
ALTER TABLE [fb].[class]  WITH CHECK ADD  CONSTRAINT [FK_studyClass_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [fb].[class] CHECK CONSTRAINT [FK_studyClass_status]
GO
ALTER TABLE [fb].[cross]  WITH CHECK ADD  CONSTRAINT [FK_cross_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [fb].[cross] CHECK CONSTRAINT [FK_cross_status]
GO
ALTER TABLE [fb].[cross]  WITH CHECK ADD  CONSTRAINT [FK_cross_study] FOREIGN KEY([studyId])
REFERENCES [fb].[study] ([id])
GO
ALTER TABLE [fb].[cross] CHECK CONSTRAINT [FK_cross_study]
GO
ALTER TABLE [fb].[designType]  WITH CHECK ADD  CONSTRAINT [FK_designType_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [fb].[designType] CHECK CONSTRAINT [FK_designType_crop]
GO
ALTER TABLE [fb].[designType]  WITH CHECK ADD  CONSTRAINT [FK_designType_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [fb].[designType] CHECK CONSTRAINT [FK_designType_status]
GO
ALTER TABLE [fb].[experiment]  WITH CHECK ADD  CONSTRAINT [FK_experiment_class] FOREIGN KEY([classId])
REFERENCES [fb].[class] ([id])
GO
ALTER TABLE [fb].[experiment] CHECK CONSTRAINT [FK_experiment_class]
GO
ALTER TABLE [fb].[experiment]  WITH CHECK ADD  CONSTRAINT [FK_experiment_designType] FOREIGN KEY([designTypeId])
REFERENCES [fb].[designType] ([id])
GO
ALTER TABLE [fb].[experiment] CHECK CONSTRAINT [FK_experiment_designType]
GO
ALTER TABLE [fb].[experiment]  WITH CHECK ADD  CONSTRAINT [FK_experiment_georeference] FOREIGN KEY([georeferenceId])
REFERENCES [cm].[georeference] ([id])
GO
ALTER TABLE [fb].[experiment] CHECK CONSTRAINT [FK_experiment_georeference]
GO
ALTER TABLE [fb].[experiment]  WITH CHECK ADD  CONSTRAINT [FK_experiment_megaEnvironment] FOREIGN KEY([megaEnvironmentId])
REFERENCES [cm].[megaEnvironment] ([id])
GO
ALTER TABLE [fb].[experiment] CHECK CONSTRAINT [FK_experiment_megaEnvironment]
GO
ALTER TABLE [fb].[experiment]  WITH CHECK ADD  CONSTRAINT [FK_experiment_season] FOREIGN KEY([seasonId])
REFERENCES [cm].[season] ([id])
GO
ALTER TABLE [fb].[experiment] CHECK CONSTRAINT [FK_experiment_season]
GO
ALTER TABLE [fb].[experiment]  WITH CHECK ADD  CONSTRAINT [FK_experiment_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [fb].[experiment] CHECK CONSTRAINT [FK_experiment_status]
GO
ALTER TABLE [fb].[experiment]  WITH CHECK ADD  CONSTRAINT [FK_experiment_study] FOREIGN KEY([studyId])
REFERENCES [fb].[study] ([id])
GO
ALTER TABLE [fb].[experiment] CHECK CONSTRAINT [FK_experiment_study]
GO
ALTER TABLE [fb].[experimentDetail]  WITH CHECK ADD  CONSTRAINT [FK_experimentDetail_customField] FOREIGN KEY([customFieldId])
REFERENCES [cm].[customField] ([id])
GO
ALTER TABLE [fb].[experimentDetail] CHECK CONSTRAINT [FK_experimentDetail_customField]
GO
ALTER TABLE [fb].[observation]  WITH CHECK ADD  CONSTRAINT [FK_observation_variable] FOREIGN KEY([variableId])
REFERENCES [on].[variable] ([id])
GO
ALTER TABLE [fb].[observation] CHECK CONSTRAINT [FK_observation_variable]
GO
ALTER TABLE [fb].[plot]  WITH CHECK ADD  CONSTRAINT [FK_plot_studyEntry] FOREIGN KEY([studyEntryId])
REFERENCES [fb].[studyEntry] ([id])
GO
ALTER TABLE [fb].[plot] CHECK CONSTRAINT [FK_plot_studyEntry]
GO
ALTER TABLE [fb].[study]  WITH CHECK ADD  CONSTRAINT [FK_study_class] FOREIGN KEY([targetClassId])
REFERENCES [fb].[class] ([id])
GO
ALTER TABLE [fb].[study] CHECK CONSTRAINT [FK_study_class]
GO
ALTER TABLE [fb].[study]  WITH CHECK ADD  CONSTRAINT [FK_study_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [fb].[study] CHECK CONSTRAINT [FK_study_crop]
GO
ALTER TABLE [fb].[study]  WITH CHECK ADD  CONSTRAINT [FK_study_megaEnvironment] FOREIGN KEY([targetMegaEnvironmentId])
REFERENCES [cm].[megaEnvironment] ([id])
GO
ALTER TABLE [fb].[study] CHECK CONSTRAINT [FK_study_megaEnvironment]
GO
ALTER TABLE [fb].[study]  WITH CHECK ADD  CONSTRAINT [FK_study_organization] FOREIGN KEY([organizationId])
REFERENCES [cm].[organization] ([id])
GO
ALTER TABLE [fb].[study] CHECK CONSTRAINT [FK_study_organization]
GO
ALTER TABLE [fb].[study]  WITH CHECK ADD  CONSTRAINT [FK_study_program] FOREIGN KEY([programId])
REFERENCES [cm].[program] ([id])
GO
ALTER TABLE [fb].[study] CHECK CONSTRAINT [FK_study_program]
GO
ALTER TABLE [fb].[study]  WITH CHECK ADD  CONSTRAINT [FK_study_season] FOREIGN KEY([targetSeasonId])
REFERENCES [cm].[season] ([id])
GO
ALTER TABLE [fb].[study] CHECK CONSTRAINT [FK_study_season]
GO
ALTER TABLE [fb].[study]  WITH CHECK ADD  CONSTRAINT [FK_study_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [fb].[study] CHECK CONSTRAINT [FK_study_status]
GO
ALTER TABLE [fb].[study]  WITH CHECK ADD  CONSTRAINT [FK_study_studyType] FOREIGN KEY([typeId])
REFERENCES [fb].[studyType] ([id])
GO
ALTER TABLE [fb].[study] CHECK CONSTRAINT [FK_study_studyType]
GO
ALTER TABLE [fb].[studyEntry]  WITH NOCHECK ADD  CONSTRAINT [FK_studyEntry_study] FOREIGN KEY([studyId])
REFERENCES [fb].[study] ([id])
GO
ALTER TABLE [fb].[studyEntry] CHECK CONSTRAINT [FK_studyEntry_study]
GO
ALTER TABLE [fb].[studyEntry]  WITH NOCHECK ADD  CONSTRAINT [FK_studyEntry_studyEntryType] FOREIGN KEY([typeId])
REFERENCES [fb].[studyEntryType] ([id])
GO
ALTER TABLE [fb].[studyEntry] CHECK CONSTRAINT [FK_studyEntry_studyEntryType]
GO
ALTER TABLE [fb].[studyEntryType]  WITH CHECK ADD  CONSTRAINT [FK_studyEntryType_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [fb].[studyEntryType] CHECK CONSTRAINT [FK_studyEntryType_status]
GO
ALTER TABLE [fb].[studyType]  WITH CHECK ADD  CONSTRAINT [FK_studyType_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [fb].[studyType] CHECK CONSTRAINT [FK_studyType_status]
GO
ALTER TABLE [gp].[germplasm]  WITH CHECK ADD  CONSTRAINT [FK_germplasm_breedingMethod] FOREIGN KEY([breedingMethodId])
REFERENCES [cm].[breedingMethod] ([id])
GO
ALTER TABLE [gp].[germplasm] CHECK CONSTRAINT [FK_germplasm_breedingMethod]
GO
ALTER TABLE [gp].[germplasm]  WITH CHECK ADD  CONSTRAINT [FK_germplasm_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [gp].[germplasm] CHECK CONSTRAINT [FK_germplasm_crop]
GO
ALTER TABLE [gp].[germplasm]  WITH CHECK ADD  CONSTRAINT [FK_germplasm_georeference] FOREIGN KEY([georeferenceId])
REFERENCES [cm].[georeference] ([id])
GO
ALTER TABLE [gp].[germplasm] CHECK CONSTRAINT [FK_germplasm_georeference]
GO
ALTER TABLE [gp].[germplasm]  WITH CHECK ADD  CONSTRAINT [FK_germplasm_germplasmType] FOREIGN KEY([typeId])
REFERENCES [gp].[germplasmType] ([id])
GO
ALTER TABLE [gp].[germplasm] CHECK CONSTRAINT [FK_germplasm_germplasmType]
GO
ALTER TABLE [gp].[germplasm]  WITH CHECK ADD  CONSTRAINT [FK_germplasm_megaEnvironment] FOREIGN KEY([megaEnvironmentId])
REFERENCES [cm].[megaEnvironment] ([id])
GO
ALTER TABLE [gp].[germplasm] CHECK CONSTRAINT [FK_germplasm_megaEnvironment]
GO
ALTER TABLE [gp].[germplasm]  WITH CHECK ADD  CONSTRAINT [FK_germplasm_organization] FOREIGN KEY([organizationId])
REFERENCES [cm].[organization] ([id])
GO
ALTER TABLE [gp].[germplasm] CHECK CONSTRAINT [FK_germplasm_organization]
GO
ALTER TABLE [gp].[germplasm]  WITH CHECK ADD  CONSTRAINT [FK_germplasm_program] FOREIGN KEY([programId])
REFERENCES [cm].[program] ([id])
GO
ALTER TABLE [gp].[germplasm] CHECK CONSTRAINT [FK_germplasm_program]
GO
ALTER TABLE [gp].[germplasm]  WITH CHECK ADD  CONSTRAINT [FK_germplasm_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [gp].[germplasm] CHECK CONSTRAINT [FK_germplasm_status]
GO
ALTER TABLE [gp].[germplasmDetail]  WITH CHECK ADD  CONSTRAINT [FK_germplasmDetail_customField] FOREIGN KEY([customFieldId])
REFERENCES [cm].[customField] ([id])
GO
ALTER TABLE [gp].[germplasmDetail] CHECK CONSTRAINT [FK_germplasmDetail_customField]
GO
ALTER TABLE [gp].[germplasmDetail]  WITH CHECK ADD  CONSTRAINT [FK_germplasmDetail_germplasm] FOREIGN KEY([germplasmId])
REFERENCES [gp].[germplasm] ([id])
GO
ALTER TABLE [gp].[germplasmDetail] CHECK CONSTRAINT [FK_germplasmDetail_germplasm]
GO
ALTER TABLE [gp].[germplasmType]  WITH CHECK ADD  CONSTRAINT [FK_germplasmType_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [gp].[germplasmType] CHECK CONSTRAINT [FK_germplasmType_status]
GO
ALTER TABLE [gp].[name]  WITH CHECK ADD  CONSTRAINT [FK_name_georeference] FOREIGN KEY([georeferenceId])
REFERENCES [cm].[georeference] ([id])
GO
ALTER TABLE [gp].[name] CHECK CONSTRAINT [FK_name_georeference]
GO
ALTER TABLE [gp].[name]  WITH CHECK ADD  CONSTRAINT [FK_name_nameType] FOREIGN KEY([typeId])
REFERENCES [gp].[nameType] ([id])
GO
ALTER TABLE [gp].[name] CHECK CONSTRAINT [FK_name_nameType]
GO
ALTER TABLE [gp].[name]  WITH CHECK ADD  CONSTRAINT [FK_name_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [gp].[name] CHECK CONSTRAINT [FK_name_status]
GO
ALTER TABLE [gp].[nameType]  WITH CHECK ADD  CONSTRAINT [FK_nameType_Crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [gp].[nameType] CHECK CONSTRAINT [FK_nameType_Crop]
GO
ALTER TABLE [gp].[nameType]  WITH CHECK ADD  CONSTRAINT [FK_nameType_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [gp].[nameType] CHECK CONSTRAINT [FK_nameType_status]
GO
ALTER TABLE [gp].[progenitorType]  WITH CHECK ADD  CONSTRAINT [FK_progenitorType_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [gp].[progenitorType] CHECK CONSTRAINT [FK_progenitorType_status]
GO
ALTER TABLE [mg].[function]  WITH CHECK ADD  CONSTRAINT [FK_function_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [mg].[function] CHECK CONSTRAINT [FK_function_status]
GO
ALTER TABLE [mg].[grant]  WITH CHECK ADD  CONSTRAINT [FK_grant_function] FOREIGN KEY([functionId])
REFERENCES [mg].[function] ([id])
GO
ALTER TABLE [mg].[grant] CHECK CONSTRAINT [FK_grant_function]
GO
ALTER TABLE [mg].[grant]  WITH CHECK ADD  CONSTRAINT [FK_grant_program] FOREIGN KEY([programId])
REFERENCES [cm].[program] ([id])
GO
ALTER TABLE [mg].[grant] CHECK CONSTRAINT [FK_grant_program]
GO
ALTER TABLE [mg].[grant]  WITH CHECK ADD  CONSTRAINT [FK_grant_role] FOREIGN KEY([roleId])
REFERENCES [mg].[role] ([id])
GO
ALTER TABLE [mg].[grant] CHECK CONSTRAINT [FK_grant_role]
GO
ALTER TABLE [mg].[grant]  WITH CHECK ADD  CONSTRAINT [FK_grant_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [mg].[grant] CHECK CONSTRAINT [FK_grant_status]
GO
ALTER TABLE [mg].[person]  WITH CHECK ADD  CONSTRAINT [FK_person_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [mg].[person] CHECK CONSTRAINT [FK_person_status]
GO
ALTER TABLE [mg].[person]  WITH CHECK ADD  CONSTRAINT [FK_person_title] FOREIGN KEY([titleId])
REFERENCES [mg].[title] ([id])
GO
ALTER TABLE [mg].[person] CHECK CONSTRAINT [FK_person_title]
GO
ALTER TABLE [mg].[person]  WITH CHECK ADD  CONSTRAINT [FK_person_user] FOREIGN KEY([userId])
REFERENCES [mg].[user] ([id])
GO
ALTER TABLE [mg].[person] CHECK CONSTRAINT [FK_person_user]
GO
ALTER TABLE [mg].[role]  WITH CHECK ADD  CONSTRAINT [FK_role_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [mg].[role] CHECK CONSTRAINT [FK_role_status]
GO
ALTER TABLE [mg].[title]  WITH CHECK ADD  CONSTRAINT [FK_title_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [mg].[title] CHECK CONSTRAINT [FK_title_status]
GO
ALTER TABLE [mg].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [mg].[user] CHECK CONSTRAINT [FK_user_status]
GO
ALTER TABLE [mg].[userByRole]  WITH CHECK ADD  CONSTRAINT [FK_userByRole_role] FOREIGN KEY([roleId])
REFERENCES [mg].[role] ([id])
GO
ALTER TABLE [mg].[userByRole] CHECK CONSTRAINT [FK_userByRole_role]
GO
ALTER TABLE [mg].[userByRole]  WITH CHECK ADD  CONSTRAINT [FK_userByRole_user] FOREIGN KEY([userId])
REFERENCES [mg].[user] ([id])
GO
ALTER TABLE [mg].[userByRole] CHECK CONSTRAINT [FK_userByRole_user]
GO
ALTER TABLE [on].[scale]  WITH CHECK ADD  CONSTRAINT [FK_scale_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [on].[scale] CHECK CONSTRAINT [FK_scale_status]
GO
ALTER TABLE [on].[trait]  WITH CHECK ADD  CONSTRAINT [FK_trait_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [on].[trait] CHECK CONSTRAINT [FK_trait_crop]
GO
ALTER TABLE [on].[trait]  WITH CHECK ADD  CONSTRAINT [FK_trait_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [on].[trait] CHECK CONSTRAINT [FK_trait_status]
GO
ALTER TABLE [on].[variable]  WITH CHECK ADD  CONSTRAINT [FK_variable_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [on].[variable] CHECK CONSTRAINT [FK_variable_crop]
GO
ALTER TABLE [on].[variable]  WITH CHECK ADD  CONSTRAINT [FK_variable_method] FOREIGN KEY([methodId])
REFERENCES [on].[method] ([id])
GO
ALTER TABLE [on].[variable] CHECK CONSTRAINT [FK_variable_method]
GO
ALTER TABLE [on].[variable]  WITH CHECK ADD  CONSTRAINT [FK_variable_scale] FOREIGN KEY([scaleId])
REFERENCES [on].[scale] ([id])
GO
ALTER TABLE [on].[variable] CHECK CONSTRAINT [FK_variable_scale]
GO
ALTER TABLE [on].[variable]  WITH CHECK ADD  CONSTRAINT [FK_variable_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [on].[variable] CHECK CONSTRAINT [FK_variable_status]
GO
ALTER TABLE [on].[variable]  WITH CHECK ADD  CONSTRAINT [FK_variable_trait] FOREIGN KEY([traitId])
REFERENCES [on].[trait] ([id])
GO
ALTER TABLE [on].[variable] CHECK CONSTRAINT [FK_variable_trait]
GO
ALTER TABLE [on].[variable]  WITH CHECK ADD  CONSTRAINT [FK_variable_variableContext] FOREIGN KEY([contextId])
REFERENCES [on].[variableContext] ([id])
GO
ALTER TABLE [on].[variable] CHECK CONSTRAINT [FK_variable_variableContext]
GO
ALTER TABLE [on].[variableContext]  WITH CHECK ADD  CONSTRAINT [FK_variableContext_crop] FOREIGN KEY([cropId])
REFERENCES [cm].[crop] ([id])
GO
ALTER TABLE [on].[variableContext] CHECK CONSTRAINT [FK_variableContext_crop]
GO
ALTER TABLE [on].[variableContext]  WITH CHECK ADD  CONSTRAINT [FK_variableContext_status] FOREIGN KEY([statusId])
REFERENCES [cm].[status] ([id])
GO
ALTER TABLE [on].[variableContext] CHECK CONSTRAINT [FK_variableContext_status]
GO
ALTER TABLE [cm].[customField]  WITH CHECK ADD  CONSTRAINT [CK_customField_dataType] CHECK  (([dataType]='B' OR [dataType]='D' OR [dataType]='R' OR [dataType]='N' OR [dataType]='C'))
GO
ALTER TABLE [cm].[customField] CHECK CONSTRAINT [CK_customField_dataType]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethod', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the breeding method' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethod', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Explanation of how a method works or its function' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethod', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short character identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethod', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The crop this method is valid for' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethod', @level2type=N'COLUMN',@level2name=N'cropId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of method' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethod', @level2type=N'COLUMN',@level2name=N'typeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethod', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines methods used by breeders to generate new germplasm' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethodType', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the breeding method type' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethodType', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Explanation of the characteristic determining if a breeding method belongs to this type' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethodType', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethodType', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines classification or grouping for breeding methods, depending on some characteristic' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'breedingMethodType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'crop', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short character identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'crop', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the crop' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'crop', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'crop', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The different crops supported in the system. Allows to filter records for a particular crop in several tables' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'crop'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'customField', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the entity owning the property' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'customField', @level2type=N'COLUMN',@level2name=N'entity'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the custom property' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'customField', @level2type=N'COLUMN',@level2name=N'field'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of values this property supports. C=Character String, N=Discrete Number, R=Real Number, D=Date/Time, B=Boolean' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'customField', @level2type=N'COLUMN',@level2name=N'dataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'customField', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Allows to fefine additional properties to entities. Values of such properties are stored in tables with the same name, plus the suffix Detail' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'customField'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'checks de data type for the custom field is valid: C, N, R, D or B' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'customField', @level2type=N'CONSTRAINT',@level2name=N'CK_customField_dataType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short character identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Common name or the used most often' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Level this reference occupies in a hierarchy. For example, a country may have hierarchy 0, and states in such country hierarchy 1' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'hierarchy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Decimal coordinate for north-south position' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'latitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Decimal coordinate for east-west position' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'longitude'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Decimal coordinate for vertical height from sea level' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'elevation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The crop this georeference is valid for' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'cropId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of georeference' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'typeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Georeference of higher hierarchy enclosing this georeference' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference', @level2type=N'COLUMN',@level2name=N'parentGeoreferenceId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores references to experimental fields, countries, locations, regions, or any other kind of geographical point' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeference'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeferenceDetail', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'String representation of the value. Correct datatype must be checked in CustomField definition' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeferenceDetail', @level2type=N'COLUMN',@level2name=N'value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The georeference owning this custom field value' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeferenceDetail', @level2type=N'COLUMN',@level2name=N'georeferenceId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The custom field associated to the value' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeferenceDetail', @level2type=N'COLUMN',@level2name=N'customFieldId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contains values of custom fields defined for georeference entities' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeferenceDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeferenceType', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the georeference type, for example: country, breeding location, etc' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeferenceType', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Explanation of the characteristic determining if a georeference belongs to this type' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeferenceType', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeferenceType', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N' Defines classification or grouping for georeferences, depending on some characteristic' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'georeferenceType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'megaEnvironment', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'megaEnvironment Acronym' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'megaEnvironment', @level2type=N'COLUMN',@level2name=N'acronym'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the megaEnvironment' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'megaEnvironment', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System level status' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'megaEnvironment', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Crop Id reference' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'megaEnvironment', @level2type=N'COLUMN',@level2name=N'cropId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'organization', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the organization' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'organization', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Code of the organization' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'organization', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'organization', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time when the record was created' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'organization', @level2type=N'COLUMN',@level2name=N'createdOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user identifier that created the record' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'organization', @level2type=N'COLUMN',@level2name=N'createdByUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The institution that was the origin of the cross' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'organization'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'program', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the program' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'program', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Alternate, and often well known, short name for the program' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'program', @level2type=N'COLUMN',@level2name=N'acronym'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Further information as purpose, objectives, etc' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'program', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference to an external database managing programs' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'program', @level2type=N'COLUMN',@level2name=N'externalReference'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The crop the program refers to' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'program', @level2type=N'COLUMN',@level2name=N'cropId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'program', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines research or institutional programs as a high level of organization for studies' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'program'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'season', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the season' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'season', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique character identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'season', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Approximate minimal date for an event to be considered happening in a season' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'season', @level2type=N'COLUMN',@level2name=N'starting'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Approximate maximum date for an event to be considered happening in a season' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'season', @level2type=N'COLUMN',@level2name=N'ending'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The crop this season is valid for' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'season', @level2type=N'COLUMN',@level2name=N'cropId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'season', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines periods for agricultural cycles' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'season'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'status', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the state' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'status', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Explanation of the effect of this status in a table record' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'status', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System level states of control for records' , @level0type=N'SCHEMA',@level0name=N'cm', @level1type=N'TABLE',@level1name=N'status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'class', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the class' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'class', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Acronym of the class' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'class', @level2type=N'COLUMN',@level2name=N'acronym'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full description about the class' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'class', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'class', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference when a record is inserted in the database' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'class', @level2type=N'COLUMN',@level2name=N'createdOn'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference of who inserts a record in the database' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'class', @level2type=N'COLUMN',@level2name=N'createdBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines a sub classification of studies and experiments' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'class'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'cross', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag to mark successful and unsuccessful crosses' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'cross', @level2type=N'COLUMN',@level2name=N'successful'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Study, usually of type crossing block, where the cross was made' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'cross', @level2type=N'COLUMN',@level2name=N'studyId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Resulting germplasm of the planned cross, regardless if it was successful or not' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'cross', @level2type=N'COLUMN',@level2name=N'germplasmId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'System level status' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'cross', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lists crosses defined in the crossing plan of a study' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'cross'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'designType', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the design type' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'designType', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Explanation of the experimental design' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'designType', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The crop this design type is valid for' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'designType', @level2type=N'COLUMN',@level2name=N'cropId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'designType', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Lists the models available to create a design for a field experiment to define how germplasm is assigned to the plots' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'designType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The study definig the entries for this experiment' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'studyId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the experiment, sometimes being similar to the name of the owning study' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short designation for the experiment' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'abbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of plots in the experiment, calculated as entries x replicates' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'plots'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines the starting number for plots. For example if it is 100 the plot numbers start at 101.' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'plotOffset'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of times the study entries appear in the experiment' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'replicates'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Design applied to the experiment' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'designTypeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Season when the experiment occurred' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'seasonId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Place or location where the experiment was conducted' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'georeferenceId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'classification at experiment level' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'classId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Environment where the experiment was conducted, likely to be linked to breeding location' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment', @level2type=N'COLUMN',@level2name=N'megaEnvironmentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines an instance of a study after a design is applied in a specific location and time' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experiment'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experimentDetail', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'String representation of the value. Correct datatype must be checked in CustomField definition' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experimentDetail', @level2type=N'COLUMN',@level2name=N'value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The experiment owning this custom field value' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experimentDetail', @level2type=N'COLUMN',@level2name=N'experimentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The custom field associated to the value' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experimentDetail', @level2type=N'COLUMN',@level2name=N'customFieldId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contains values of custom fields defined for experiment entities' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'experimentDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'plot', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The germplasm entry assigned in the plot, ' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'plot', @level2type=N'COLUMN',@level2name=N'studyEntryId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The experiment the plot is associated with' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'plot', @level2type=N'COLUMN',@level2name=N'experimentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The plot number' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'plot', @level2type=N'COLUMN',@level2name=N'number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The replicate number' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'plot', @level2type=N'COLUMN',@level2name=N'replicate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The subblock, being a lower level division of a replicate. Optional depending on design type' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'plot', @level2type=N'COLUMN',@level2name=N'subBlock'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Y axis of a cartesian distribution of plots.  Optional depending on design type' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'plot', @level2type=N'COLUMN',@level2name=N'row'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The X axis of a cartesian distribution of plots.  Optional depending on design type' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'plot', @level2type=N'COLUMN',@level2name=N'column'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Represents plots as the incidence of a germplasm in a point in a field' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'plot'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the study' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short designation for the study' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'abbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Study''s objective or explanation of its purpose' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date when the study started' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'starting'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date when the study ended' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'ending'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of germplasms considered for the study. Also called the test list' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'tests'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of germplasms used as control checks. Also called the check list' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'checks'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The total number of germpalsm used in the study calculated as tests + checks. Also called the entry list' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'totalEntries'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Starting point for the check frequency counting in the test list. For example, if checkOffset is 3 and checkFrequency is 10, checks are added in places 13, 23, 33, etc., of the test list' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'checkOffset'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines every how many tests a check insertion occurs in the test list. For example, if checkFrequency is 5, check are placed in positions 5,10,15, etc., of the test list' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'checkFrequency'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of checks inserted from the check list when a check insertion occurs. For example, if checkFrequency is 10 and checkInsertions is 2, checks 1 and 2 are placed in position 10, 3 and 4 in position 20 and so on' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'checkInsertions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of experiments associated to the study' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'experiments'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The program associated to this study' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'programId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of study' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'typeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The creator of the study, usually the person responsible of it or having its rights' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'ownerUserId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The crop of the germplam used in the study' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'cropId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Mega environment the study was designed for' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'targetMegaEnvironmentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The season when experiments in this study are planned to be carried out. Optionally may represent the current or coming season when the study was created' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'targetSeasonId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The main classification for experiments in a study, this is not a rule and some experiments mauy have a different class' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study', @level2type=N'COLUMN',@level2name=N'targetClassId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines a procedure for a specific-purpose task or research' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'study'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntry', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Specific order for the entries' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntry', @level2type=N'COLUMN',@level2name=N'sequence'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The study containing the entry' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntry', @level2type=N'COLUMN',@level2name=N'studyId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Germplasm defined for the entry' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntry', @level2type=N'COLUMN',@level2name=N'germplasmId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of entry' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntry', @level2type=N'COLUMN',@level2name=N'typeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The list of germplasm entries to be used in each experiment of the study. This list is the sum of tests and checks' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntry'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntryType', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the entry type. For example test, check, etc.' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntryType', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description of why a germplasm should be of certain type within a study, depending on its role or function.' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntryType', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntryType', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines the role a germplasm plays in an experiment or study' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyEntryType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyType', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name of the study type' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyType', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Description of the characteristics of a study of a given type' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyType', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyType', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Classification for studies' , @level0type=N'SCHEMA',@level0name=N'fb', @level1type=N'TABLE',@level1name=N'studyType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'genealogy', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Germplasm to have a relationship with a parent defined' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'genealogy', @level2type=N'COLUMN',@level2name=N'germplasmId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The germplasm parent for the specified germplasm' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'genealogy', @level2type=N'COLUMN',@level2name=N'progenitorId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of progenitor' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'genealogy', @level2type=N'COLUMN',@level2name=N'progenitorTypeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Keeps references between germplasm and its parents and their type, allowing to assemble the genealogy tree of a germplasm' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'genealogy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Legacy identifier of the germplasm. May be duplicated across different crops' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'gid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of germpalsm' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'typeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The crop of the germplasm' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'cropId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The breeding method used to produce the germplasm' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'breedingMethodId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The georeference where the germplasm was produced' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'georeferenceId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Associates a germplasm with its taxonomy information' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'speciesId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The program origin of the germplasm' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'programId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The institution origin of the germplasm' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'organizationId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The mega environment associated with the germplasm' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm', @level2type=N'COLUMN',@level2name=N'megaEnvironmentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Describes germplasm available in the database' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasmDetail', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'String representation of the value. Correct datatype must be checked in CustomField definition' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasmDetail', @level2type=N'COLUMN',@level2name=N'value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The germplasm owning this custom field value' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasmDetail', @level2type=N'COLUMN',@level2name=N'germplasmId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The custom field associated to the value' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasmDetail', @level2type=N'COLUMN',@level2name=N'customFieldId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contains values of custom fields defined for germplasm entities' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasmDetail'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasmType', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the germpalsm type' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasmType', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Explanation of the characteristic determining if a germpalsm belongs to this type' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasmType', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasmType', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines classification or grouping for germplasm, depending on some characteristic' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'germplasmType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'name', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The name to refer a germplasm' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'name', @level2type=N'COLUMN',@level2name=N'value'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Short version of the name, a breeder may choose to use it in pedigree instead of the regular name' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'name', @level2type=N'COLUMN',@level2name=N'abbreviation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Flag to mark the name to be used to build germplasm''s pedigree. One and only one name by germplasm can have this flag ON' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'name', @level2type=N'COLUMN',@level2name=N'preferred'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The type of name' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'name', @level2type=N'COLUMN',@level2name=N'typeId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The germplasm a name belongs to' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'name', @level2type=N'COLUMN',@level2name=N'germplasmId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference of the place where the name was generated' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'name', @level2type=N'COLUMN',@level2name=N'georeferenceId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'name', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Stores the different names to refer a particular germplasm' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'nameType', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Designation of the name type' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'nameType', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Explanation of the characteristic determining if a germplasm name belongs to this type' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'nameType', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Unique character identifier' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'nameType', @level2type=N'COLUMN',@level2name=N'code'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'nameType', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines classification or grouping for germplasm names, depending on some characteristic' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'nameType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Entity identifier' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'progenitorType', @level2type=N'COLUMN',@level2name=N'id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the progenitor type' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'progenitorType', @level2type=N'COLUMN',@level2name=N'name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Explanation of the case or cases in which a progenitor is of this type' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'progenitorType', @level2type=N'COLUMN',@level2name=N'description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The system level status' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'progenitorType', @level2type=N'COLUMN',@level2name=N'statusId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Defines classification or grouping for progenitors, depending on some characteristic' , @level0type=N'SCHEMA',@level0name=N'gp', @level1type=N'TABLE',@level1name=N'progenitorType'
GO
USE [master]
GO
ALTER DATABASE [CERES] SET  READ_WRITE 
GO

