import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ceres-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

	private menuData:any = {
		General: [
			{link:'/study', caption:'Studies', active: 'active'}
		],
		Management: [
			{link:'/status', caption:'Status', active: 'active'},
			{link:'/program', caption:'Program', active: 'active'},
			{link:'/crop', caption:'Crop', active: 'active'}
		]
	};
	private menuGroups: String[];

	constructor(private router:Router){
		this.menuGroups = Object.getOwnPropertyNames(this.menuData);
	}

  login(){
  	console.log('Login not yet implemented!');
  	window.alert('Not implemented yet. :(');
  }

  goHome(){
  	this.router.navigate(['dashboard']);
  }

}
