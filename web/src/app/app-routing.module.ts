import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './cm/dashboard/dashboard.component';

const routes:Routes = [
	{
		path: '',
		redirectTo: '/dashboard',
		pathMatch: 'full'
	},{
		path: 'dashboard',
		component: DashboardComponent
	},{ 
		path: 'status',
		loadChildren: './cm/status/status.module#StatusModule'

	},{ 
		path: 'program',
		loadChildren: './cm/program/program.module#ProgramModule'

	},{ 
		path: 'study',
		loadChildren: './stdy/studies.module#StudiesModule'
	},{ 
		path: 'crop',
		loadChildren: './cm/crop/crop.module#CropModule'
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule{ }