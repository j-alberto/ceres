import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ProgramService } from '../../../srv/program.service';
import { Router } from '@angular/router';
import { GridOptions } from 'ag-grid/main';

@Component({
  selector: 'crs-program-list',
  templateUrl: './program-list.component.html',
  styleUrls: ['./program-list.component.css']
})
export class ProgramListComponent implements OnInit { 

	gridOptions: GridOptions;
  columns:any[] = [
    { headerName:'Name', field: 'name', pinned: true },
    { headerName:'Acronym', field: 'acronym' },
    { headerName:'External Reference', field: 'externalReference' },
    { headerName:'Description', field: 'description' },
    { headerName:'Crop', field: 'crop' },
    { headerName:'Status', field: 'status' },
    { headerName:'Creation Date', field: 'createdOn' },
    { headerName:'Creator', field: 'createdByUserId' },
	];

  constructor(private programService:ProgramService,
  		private router:Router) {
    
    this.gridOptions = <GridOptions>{};
  }

   ngOnInit() {
  	this.programService.getPrograms()
  		.then(programs => {
  			this.gridOptions.api.setRowData(programs);
  		});
  }


	ngAfterViewInit() {
	}

  private addNew() {
    this.router.navigate(['program/register']);
  }

  private goToDetail(){
		this.router.navigate(['program/'+this.gridOptions.api.getSelectedRows()[0].id]);
  }
  

}
