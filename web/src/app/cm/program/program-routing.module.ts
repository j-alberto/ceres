import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramComponent } from './program.component';
import { ProgramListComponent } from './program-list/program-list.component';
import { ProgramDetailComponent } from './program-detail/program-detail.component';

const routes:Routes = [
	{
		path: '',
		component: ProgramComponent,
		children: [
			{
				path: '',
				component: ProgramListComponent
		  },{
				path: 'register',
				component: ProgramDetailComponent
		  },{
				path: ':id',
				component: ProgramDetailComponent
		  }
		]}];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
  	RouterModule
  ]
})
export class ProgramRoutingModule { }
