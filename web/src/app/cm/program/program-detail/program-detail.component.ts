import { ElementRef,Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { NgForm } from '@angular/forms';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Rx';

import { ProgramService } from '../../../srv/program.service';
import { StatusService } from '../../../srv/status.service';
import { CropService } from '../../../srv/crop.service';
import { Program } from '../../../mdl/program';
import { Status } from '../../../mdl/status';
import { Crop } from '../../../mdl/crop';

@Component({
  selector: 'crs-program-detail',
  templateUrl: './program-detail.component.html',
  styleUrls: ['./program-detail.component.css']
})
export class ProgramDetailComponent implements OnInit, AfterViewInit {

	private program: Program;
  private tmpProg: Program = new Program();
	private snackConfig: MdSnackBarConfig;
	private isEditing: boolean = false;
  private statuses: Status[];
  private crops: Crop[];
	private formErrors = {
  	name:'',
  	acronym:'',
    description:'',
    crop:'',
    status:''
  };

  constructor(private route:ActivatedRoute,
			private location: Location,
			private programService: ProgramService,
      private snackBar: MdSnackBar,
      private statusService:StatusService,
      private cropService:CropService) { 

    this.snackConfig = new MdSnackBarConfig();
    this.snackConfig.duration = 2500;

    this.program = new Program();
	}

	@ViewChild('programForm') vcProgramForm: NgForm;
  @ViewChild('name') vcName: ElementRef;
  @ViewChild('acronym') vcAcronym: ElementRef;

  validationMessages = {
	  'name': {
	    'required':	'A name is required.',
	    'pattern':  'Use only letters, numbers and [ . - _ ]',
	  },
	  'acronym': {
	    'required': 'An acronym is required.',
      'pattern':  'Use only letters, numbers and [ . - _ ]',
	  },
    'description': {
      'pattern':  'Use only letters, numbers and [ . - _ ]',
    },
    
    'externalReference':{
      'pattern': 'Only numbers are allowed'
    }
  };

  ngOnInit() {
	  this.isEditing = !this.location.isCurrentPathEqualTo('/program/register');
		
    if(this.isEditing) {
				this.retrieveProgram();
		}

		this.vcProgramForm.form.valueChanges.subscribe(() => {
      this.sanitizeModel();
      this.showErrors();
    });

    Observable.forkJoin(
        this.statusService.getStatuses(),
        this.cropService.getCrops()
    ).subscribe(results=>{
      this.statuses = results[0];
      this.crops = results[1];
      this.program = this.tmpProg;
    });
  }

  ngAfterViewInit() {
    if(this.isEditing)
      this.vcName.nativeElement.focus(); else this.vcAcronym.nativeElement.focus();
  }

  private showErrors() {
    for(let controlName in this.vcProgramForm.form.controls) {
      this.formErrors[controlName] = '';
      let control = this.vcProgramForm.form.controls[controlName];
      
      if(control.dirty && control.invalid)
        for(let errKey in control.errors) {
          this.formErrors[controlName] = this.validationMessages[controlName][errKey];
        }
    
    }
  }

  /**
  * Search the program given by the id param in the URL. The result goes to
  tmpProg. This is assigned to model object 'program' after Select components have
  their corresponding Options. 
  */
  private retrieveProgram() {
    this.route.params
      .switchMap((params:Params) => this.programService.getProgram(+params['id']))
      .subscribe((program) => {
        if (program) {
          this.tmpProg = program;
        }else {
          this.tmpProg = new Program();
          this.isEditing = false;
        }});
  }

  private sanitizeModel(){
  	const oneBlankExpr = /\s{2,}/g
  	for(let attr in this.formErrors) {
  		if(this.program[attr] && isNaN(this.program[attr]))
  			this.program[attr] = this.program[attr].trim().replace(oneBlankExpr, ' ');
  	}
  }

  goBack(){
  	this.location.back();
  }

  onSubmit() {
    if(this.isEditing) {
      this.programService.updateProgram(this.program)
        .then((prog) => {
          this.snackBar.open('Program ['+prog.name+'] changed.','Done'
            ,this.snackConfig);
          console.log('program updated');
          this.goBack();
        });
    } else {
      this.programService.createProgram(this.program)
        .then((prog) => {
          this.snackBar.open('Program ['+prog.name+'] added.','Done'
            ,this.snackConfig);
          console.log('new program saved');
          this.goBack();
        });
    }
  }

}
