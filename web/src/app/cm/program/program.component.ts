import { Component, OnInit } from '@angular/core';
import { ProgramService } from '../../srv/program.service';

@Component({
  selector: 'crs-program',
  templateUrl: './program.component.html'
})
export class ProgramComponent{
	constructor (private programService: ProgramService){}
}
