import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule, MdSnackBar } from '@angular/material';
import { AgGridModule } from 'ag-grid-ng2/main';
import { FlexLayoutModule} from '@angular/flex-layout';

import { ProgramListComponent } from './program-list/program-list.component';
import { ProgramComponent } from './program.component';
import { ProgramRoutingModule } from './program-routing.module';
import { ProgramDetailComponent } from './program-detail/program-detail.component';
import { ServiceModule } from '../../srv/service.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProgramRoutingModule,
    MaterialModule.forRoot(),
    AgGridModule.withComponents([]),
    FlexLayoutModule.forRoot(),
    ServiceModule
  ],
  declarations: [
  	ProgramComponent,
  	ProgramListComponent,
  	ProgramDetailComponent
  ],
  exports: [
  	ProgramComponent
  ]
})
export class ProgramModule { }
