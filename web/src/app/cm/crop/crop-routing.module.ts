import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CommonModule } from '@angular/common';
import { CropComponent } from './crop.component';
import { CropListComponent } from './crop-list/crop-list.component';
import { CropDetailComponent} from './crop-detail/crop-detail.component';

const routes:Routes = [
	{
		path: '',
		component: CropComponent,
		children: [
			{
				path: '',
				component: CropListComponent
		  },{
				path: 'register',
				component: CropDetailComponent
		  },{
				path: ':id',
				component: CropDetailComponent
		  }
		]}];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
  	RouterModule
  ]
})
export class CropRoutingModule { }
