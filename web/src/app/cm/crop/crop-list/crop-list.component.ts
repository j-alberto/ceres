import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GridOptions } from 'ag-grid/main';

import { CropService } from '../../../srv/crop.service';

@Component({
  selector: 'crs-crop-list',
  templateUrl: './crop-list.component.html',
  styleUrls: ['./crop-list.component.css']
})
export class CropListComponent implements OnInit {

	gridOptions: GridOptions = <GridOptions>{};
  columns:any[] = [
		{	headerName:'Identifier', field: 'id' },
    { headerName:'Name', field: 'name' },
    { headerName:'Code', field: 'code' },
    { headerName:'Status', field: 'status' }
	];

  constructor(private cropService: CropService, private router:Router) { }

  ngOnInit() {
    this.cropService.getCrops()
      .then(crops => {
        this.gridOptions.api.setRowData(crops);
      });
  }

    private addNew() {
    this.router.navigate(['crop/register']);
  }

  private goToDetail(){
    this.router.navigate(['crop/',this.gridOptions.api.getSelectedRows()[0].id]);
  }
  

}
