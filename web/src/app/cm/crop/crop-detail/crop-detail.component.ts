import { ElementRef,Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { NgForm } from '@angular/forms';
import 'rxjs/add/operator/switchMap';

import { Crop } from '../../../mdl/crop';
import { Status } from '../../../mdl/status';
import { CropService } from '../../../srv/crop.service';
import { StatusService } from '../../../srv/status.service';

@Component({
  selector: 'crs-crop-detail',
  templateUrl: './crop-detail.component.html',
  styleUrls: ['./crop-detail.component.css']
})
export class CropDetailComponent implements OnInit {

	private crop: Crop;
	private snackConfig: MdSnackBarConfig;
	private isEditing: boolean = false;
   private statusId:number;
	private formErrors = {
  	name:'',
  	code:'',
  	status:'',
  };
  private statuses:Status[];

  @ViewChild('cropsForm') vcCropForm: NgForm;
  @ViewChild('name') vcName: ElementRef;
  @ViewChild('code') vcCode: ElementRef;

  validationMessages = {
	  'name': {
	    'required':	'A name is required.',
	    'pattern':  'Use only letters, numbers and [ . - _ ]',
	  },
	  'code': {
	    'required': 'A code is required.',
      'pattern':  'Use only letters, numbers and [ . - _]',
	  }
  };

  constructor(private route:ActivatedRoute,
			private location: Location,
			private cropService: CropService,
      private statusService:StatusService,
      private snackBar: MdSnackBar) { 
  	this.crop = new Crop();
  	this.crop.name = this.crop.code = '';
    this.snackConfig = new MdSnackBarConfig();
    this.snackConfig.duration = 3500;
	}


    ngOnInit() {
		  this.isEditing = !this.location.isCurrentPathEqualTo('/crop/register');
			if(this.isEditing) {
					this.route.params
						.switchMap((params:Params) => this.cropService.getCrop(+params['id']))
						.subscribe((crop) => {
							if (crop)
								this.crop = crop;
							else {
								this.crop = new Crop();
								this.isEditing = false;
							}});
			}
			this.vcCropForm.form.valueChanges.subscribe(() => {
	      for(let controlName in this.vcCropForm.form.controls) {
	        this.formErrors[controlName] = '';
	        let control = this.vcCropForm.form.controls[controlName];
	        
	        if(control.dirty && control.invalid)
	          for(let errKey in control.errors) {
	            this.formErrors[controlName] = this.validationMessages[controlName][errKey];
	          }
	      }
			});

      this.statusService.getStatuses()
      .then(statuses => {
        this.statuses = statuses;
              this.statusId = this.crop.statusId;
      });
	  }

  ngAfterViewInit() {
  	if(this.isEditing)
      this.vcName.nativeElement.focus(); else this.vcCode.nativeElement.focus();
  }

  goBack(){
  	this.location.back();
  }

  onSubmit() {
    this.crop.statusId = this.statusId;
    if(this.isEditing) {
      this.cropService.updateCrop(this.crop)
        .then((crop) => {
          this.snackBar.open('Crop ['+crop.name+'] changed.','Done'
            ,this.snackConfig);
          console.log('Crop updated');
          this.goBack();
        });
    } else {
      this.cropService.createCrop(this.crop)
        .then((crop) => {
          this.snackBar.open('Crop ['+crop.name+'] added.','Done'
            ,this.snackConfig);
          console.log('new Crop saved');
          this.goBack();
        });
    }
  }

}
