import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule, MdSnackBar } from '@angular/material';
import { AgGridModule } from 'ag-grid-ng2/main';
import { FlexLayoutModule} from '@angular/flex-layout';

import { CropComponent } from './crop.component';
import { CropDetailComponent } from './crop-detail/crop-detail.component';
import { CropListComponent } from './crop-list/crop-list.component';
import { CropRoutingModule } from './crop-routing.module';
import { ServiceModule } from '../../srv/service.module';

@NgModule({
  imports: [
    CommonModule,
    CropRoutingModule,
    FormsModule,
    MaterialModule.forRoot(),
    AgGridModule.withComponents([]),
    FlexLayoutModule.forRoot(),
    ServiceModule
  ],
  declarations: [
  	CropComponent,
  	CropDetailComponent,
  	CropListComponent
  ],
  exports: [
  	CropComponent
  ]
})
export class CropModule { }
