import { Component, OnInit } from '@angular/core';

import { CropService } from '../../srv/crop.service';

@Component({
  selector: 'crs-crop',
  templateUrl: './crop.component.html'
})
export class CropComponent {

  constructor(private cropService: CropService) { }

}
