import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StatusService } from '../../../srv/status.service';
import { GridOptions } from 'ag-grid/main';

@Component({
  selector: 'crs-status-list',
  templateUrl: './status-list.component.html',
  styleUrls: ['./status-list.component.css']
})
export class StatusListComponent implements OnInit {

  private gridOptions: GridOptions;
  private columns:any[] = [
    {  headerName:'Identifier', field: 'id' },
    { headerName:'Name', field: 'name' },
    { headerName:'Description', field: 'description' }
  ];

  constructor(private statusService:StatusService, 
      private router: Router) { 
    this.gridOptions = <GridOptions>{};
  }

  ngOnInit() {
  	this.statusService.getStatuses()
  		.then(statuses => {
  			this.gridOptions.api.setRowData(statuses);
  		});
  }

  private addNew() {
    this.router.navigate(['status/register']);
  }

  private goToDetail(){
    this.router.navigate(['status/'+this.gridOptions.api.getSelectedRows()[0].id]);
  }
}
