import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StatusComponent } from './status.component';
import { StatusListComponent } from './status-list/status-list.component';
import { StatusDetailComponent } from './status-detail/status-detail.component';

const routes: Routes = [
	{	path: '',
		component: StatusComponent,
		children: [
			{
				path: 'register',
				component: StatusDetailComponent
			},{
				path: ':id',
				component: StatusDetailComponent
			},
			{	path: '',
				component: StatusListComponent
			}
		]
	}
];


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
  	RouterModule
  ]
})
export class StatusRoutingModule { }
