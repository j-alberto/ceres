import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material'
import { FlexLayoutModule} from '@angular/flex-layout';
import { AgGridModule } from 'ag-grid-ng2/main';

import { ServiceModule } from '../../srv/service.module';
import { StatusComponent } from './status.component';
import { StatusListComponent } from './status-list/status-list.component';
import { StatusDetailComponent } from './status-detail/status-detail.component';
import { StatusRoutingModule } from './status-routing.module';



@NgModule({
	imports: [CommonModule, 
		FormsModule,
		HttpModule,
		MaterialModule.forRoot(),
		FlexLayoutModule.forRoot(),
		AgGridModule.withComponents([]),
		StatusRoutingModule,
		ServiceModule
	],
	declarations: [
		StatusComponent,
		StatusListComponent,
		StatusDetailComponent
	],
	exports: [StatusComponent]
})
export class StatusModule{ }