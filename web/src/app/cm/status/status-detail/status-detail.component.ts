import { ElementRef,Component, OnInit, AfterViewInit, AfterViewChecked,ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { NgForm } from '@angular/forms';
import 'rxjs/add/operator/switchMap';

import { Status } from '../../../mdl/status';
import { StatusService } from '../../../srv/status.service';

@Component({
  selector: 'app-status-detail',
  templateUrl: './status-detail.component.html',
  styleUrls: ['./status-detail.component.css']
})
export class StatusDetailComponent implements OnInit, AfterViewInit, AfterViewChecked {

  constructor(private route:ActivatedRoute,
			private location: Location,
			private statusService: StatusService,
      private snackBar: MdSnackBar) { 
  	this.status = new Status();
  	this.status.name = this.status.description = '';
    this.snackConfig = new MdSnackBarConfig();
    this.snackConfig.duration = 2500;
	}

	@ViewChild('statusForm') vcStatusForm: NgForm;
  @ViewChild('description') vcDescription: ElementRef;
  @ViewChild('name') vcName: ElementRef;

  private status: Status;
  private isEditing: boolean = false;
  private formErrors = {
  	name:'',
  	description:''
  };
  private snackConfig: MdSnackBarConfig;

  validationMessages = {
	  'name': {
	    'required':	'A name is required.',
	    'pattern':  'Use only letters, numbers and [ . - _ ]',
	  },
	  'description': {
	    'required': 'A description is required.',
      'pattern':  'Use only letters, numbers and [ . - _ ]',
	  }
  };

  ngOnInit() {
	  this.isEditing = !this.location.isCurrentPathEqualTo('/status/register');
		if(this.isEditing) {
				this.route.params
					.switchMap((params:Params) => this.statusService.getStatus(+params['id']))
					.subscribe((status) => {
						if (status)
							this.status = status;
						else {
							this.status = new Status();
							this.isEditing = false;
						}});
		}
		this.vcStatusForm.form.valueChanges.subscribe(() => {
			this.sanitizeModel();
      for(let controlName in this.vcStatusForm.form.controls) {
        this.formErrors[controlName] = '';
        let control = this.vcStatusForm.form.controls[controlName];
        
        if(control.dirty && control.invalid)
          for(let errKey in control.errors) {
            this.formErrors[controlName] = this.validationMessages[controlName][errKey];
          }
      }
		});
  }

  private sanitizeModel(){
  	const oneBlankExpr = /\s{2,}/g
  	for(let attr in this.formErrors) {
  		if(this.status[attr])
  			this.status[attr] = this.status[attr].trim().replace(oneBlankExpr, ' ');
  	}
  }

  ngAfterViewInit() {
  	if(this.isEditing)
      this.vcDescription.nativeElement.focus(); else this.vcName.nativeElement.focus();
  }

  ngAfterViewChecked() {

  }

  goBack(){
  	this.location.back();
  }

  onSubmit() {
    if(this.isEditing) {
      this.statusService.updateStatus(this.status)
        .then((stat) => {
          this.snackBar.open('Status ['+stat.name+'] changed.','Done'
            ,this.snackConfig);
          console.log('status updated');
          this.goBack();
        });
    } else {
      this.statusService.createStatus(this.status)
        .then((stat) => {
          this.snackBar.open('Status ['+stat.name+'] added.','Done'
            ,this.snackConfig);
          console.log('new status saved');
          this.goBack();
        });
    }
  }

}
