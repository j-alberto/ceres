import { Component, OnInit } from '@angular/core';
import { GridOptions } from 'ag-grid/main';

declare var Handsontable:any;

@Component({
  selector: 'crs-study-observations',
  templateUrl: './study-observations.component.html',
  styleUrls: ['./study-observations.component.css']
})
export class StudyObservationsComponent implements OnInit {

  private observationsGridOptions:GridOptions;
  private observations: any[] = [];

  private obvervationsColumns:any[] = [
    { headerName:'experiment', field: 'experiment'},
    { headerName:'plot', field: 'plot'},
    { headerName:'PH_cm', field: 'phcm'},
    { headerName:'PH_cm1', field: 'phcm1'},
    { headerName:'PH_cm2', field: 'phcm2'},
    { headerName:'PH_cm3', field: 'phcm3'},
    { headerName:'PH_cm4', field: 'phcm4'},
    { headerName:'PH_cm5', field: 'phcm5'},
    { headerName:'PH_cm6', field: 'phcm6'},
    { headerName:'PH_cm7', field: 'phcm7'},
    { headerName:'PH_cm8', field: 'phcm8'},
    { headerName:'PH_cm9', field: 'phcm9'},
    { headerName:'PH_cm10', field: 'phcm10'},
    { headerName:'PH_cm11', field: 'phcm11'},
    { headerName:'PH_cm12', field: 'phcm12'},
    { headerName:'PH_cm13', field: 'phcm13'},
    { headerName:'PH_cm14', field: 'phcm14'},
  ];

  constructor() { 
  	for (var i = 0; i <= 10; i++) {
  		this.observations.push(
  			{plot:i, experiment:'experiment_'+(i/5000+1).toFixed(0),
  				phcm:135+i%13, phcm1:135+i%22, phcm2:135+i%19, phcm3:135+i%18, phcm4:135+i%17,
  				phcm5:135+i%16, phcm6:135+i%15, phcm7:135+i%14, phcm8:135+i%13, phcm9:135+i%10,
  				phcm10:135+i%8, phcm11:135+i%9, phcm12:135+i%10, phcm13:135+i%11, phcm14:135+i%12,
  			});
		}
		this.observationsGridOptions = <GridOptions>{};
		this.observationsGridOptions.rowData = this.observations;
	}

	ngOnInit(){}

}
