import { Component, OnChanges, Input } from '@angular/core';
import { GridOptions } from 'ag-grid/main';

import { Experiment } from '../../mdl/experiment';
import { Study } from '../../mdl/study';

@Component({
  selector: 'crs-study-experiments',
  templateUrl: './study-experiments.component.html',
  styleUrls: ['./study-experiments.component.css']
})
export class StudyExperimentsComponent implements OnChanges {

  private experiments: Experiment[] = [];
  private experimentsGridOptions:GridOptions;
  
  private experimentsColumns:any[] = [
    { headerName:'Name', field: 'name', pinned:true},
    { headerName:'Abbreviation', field: 'coabbreviationde'},
    { headerName:'Status', field: 'status'},
    { headerName:'Location', field: 'georeference'},
    { headerName:'Season', field: 'season'},
    { headerName:'Design', field: 'designType'},
    { headerName:'Replicates', field: 'replicates'},
    { headerName:'Plots', field: 'plots'},
    { headerName:'Plot Offset', field: 'plotOffset'},
    { headerName:'Class', field: 'class'},
    { headerName:'Mega Environment', field: 'megaEnvironment'},
    { headerName:'Owner', field: 'owner'},
    { headerName:'Creator', field: 'createdBy'},
    { headerName:'Date Creation', field: 'createdOn'},
  ];
  
  @Input()
  private study:Study;

  constructor() {
  	this.experimentsGridOptions = <GridOptions>{};
  }

  ngOnChanges() {
    if(this.experimentsGridOptions.api)
      this.experimentsGridOptions.api.setRowData(this.study.experiments);
  }

}
