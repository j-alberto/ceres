import { Component } from '@angular/core';

import { StudyService } from '../srv/study.service';

@Component({
	selector: 'studies',
  templateUrl: './studies.component.html',
  styleUrls: ['./studies.component.css']
})
export class StudiesComponent { 
	constructor(private studyService:StudyService){}
}
