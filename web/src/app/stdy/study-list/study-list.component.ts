import { Component, Output, OnInit, EventEmitter } from '@angular/core';
import { GridOptions, IDatasource } from 'ag-grid/main';
import { Router } from '@angular/router';

import { StudyService } from '../../srv/study.service';
import { Study } from '../../mdl/study';

@Component({
  selector: 'crs-study-list',
  templateUrl: './study-list.component.html',
  styleUrls: ['./study-list.component.css']
})
export class StudyListComponent implements OnInit {

  private gridOptions: GridOptions;
    columns:any[] = [
      { field: 'name', headerName: 'Name', pinned:true},
      { field: 'crop', headerName: 'Crop'},
      { field: 'abbreviation', headerName: 'Abbreviation'},
      { field: 'description', headerName: 'Description'},
      { field: 'type', headerName: 'Type'},
      { field: 'targetClass', headerName: 'Class'},
      { field: 'targetMegaEnvironment', headerName: 'Mega Environment'},
      { field: 'targetSeason', headerName: 'Season'},
      { field: 'starting', headerName: 'Start Date'},
      { field: 'ending', headerName: 'End Date'},
      { field: 'program', headerName: 'Program'},
      { field: 'organization', headerName: 'Organization'},
      { field: 'tests', headerName: 'Tests'},
      { field: 'checks', headerName: 'Checks'},
      { field: 'totalEntries', headerName: 'Total Entries'},
      { field: 'checkOffset', headerName: 'Check Offset'},
      { field: 'checkFrequency', headerName: 'Check Frequency'},
      { field: 'checkInsertions', headerName: 'Check Insertions'},
      { field: 'totalExperiments', headerName: 'Experiments'},
      { field: 'createdOn', headerName: 'Creation Date'},
      { field: 'createdBy', headerName: 'Creator Id'},
      { field: 'ownerUserId', headerName: 'Owner Id'},
      { field: 'status', headerName: 'Status'},
    ];

  @Output() studySelected:EventEmitter<Study> = new EventEmitter();

  constructor(private studyService: StudyService,
      private router:Router) { 
	}

  ngOnInit() {
    let thiz = this;

    this.gridOptions = <GridOptions>{};
    this.gridOptions.paginationPageSize = 200;
    this.gridOptions.rowModelType = 'pagination';
    this.gridOptions.datasource = {
      getRows(params) {
        let size = thiz.gridOptions.paginationPageSize;
        let currPage = Math.floor(params.startRow/size);

        thiz.studyService.getStudies(currPage,size)
          .then(page => {
            params.successCallback(page.content, page.totalElements);
          });
      }};
  }

  private goToDetail() {
    this.router.navigate(['study/'+this.gridOptions.api.getSelectedRows()[0].id]);
  }

  private setSelected() {
    let study = this.gridOptions.api.getSelectedRows()[0];
    this.studySelected.emit(study);
  }
}

  