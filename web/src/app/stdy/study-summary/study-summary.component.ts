import { Component, Input, OnInit } from '@angular/core';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { Study } from '../../mdl/study';

@Component({
  selector: 'crs-study-summary',
  templateUrl: './study-summary.component.html',
  styleUrls: ['./study-summary.component.css']
})
export class StudySummaryComponent implements OnInit {


  @Input()
	private study:Study;
	private snackConfig: MdSnackBarConfig;

  constructor(private snackBar: MdSnackBar) {
  }

  ngOnInit() {
  	this.snackConfig = new MdSnackBarConfig();
  	this.snackConfig.duration = 3000;
  }

  notYet() {
  	 this.snackBar.open('Not Implemented Yet', 'Understood :('
            ,this.snackConfig);
  }

}
