import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Study } from '../../mdl/study';

@Component({
  templateUrl: './study-home.component.html',
  styleUrls: ['./study-home.component.css']
})
export class StudyHomeComponent implements OnInit {

  private studySelection:Study;

  constructor(private router: Router) {
  	this.studySelection = new Study();
  }

  ngOnInit() { }

  private newStudy() {
   this.router.navigate(['study/register']);
  }

}
