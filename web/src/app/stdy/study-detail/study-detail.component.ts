import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { StudyService } from '../../srv/study.service';
import { Study } from '../../mdl/study';

@Component({
  selector: 'crs-study-detail',
  templateUrl: './study-detail.component.html',
  styleUrls: ['./study-detail.component.css']
})
export class StudyDetailComponent implements OnInit {

  constructor(private location: Location,
  		private router: Router,
      private route: ActivatedRoute,
      private studyService: StudyService) {
  }

  private study: Study;

  ngOnInit() {
    this.route.params
          .switchMap((params:Params) => this.studyService.getStudy(+params['id']))
          .subscribe((study) => {
            if (study)
              this.study = study;
            else {
              this.study = new Study();
            }});
  }

  private edit() {
   this.router.navigate(['study/:id/edit']);
  }
  private back() {
   this.location.back();
   //this.router.navigate(['studies']);
  }

}
