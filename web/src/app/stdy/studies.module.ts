import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule, MdSnackBar } from '@angular/material'
import { AgGridModule } from 'ag-grid-ng2/main';
import { FlexLayoutModule} from '@angular/flex-layout';

import { ServiceModule } from '../srv/service.module';
import { StudiesComponent } from './studies.component';
import { StudyWizardComponent } from './study-wizard/study-wizard.component';
import { StudyDetailComponent } from './study-detail/study-detail.component';
import { StudyHomeComponent } from './study-home/study-home.component';
import { StudyListComponent } from './study-list/study-list.component';
import { StudySummaryComponent } from './study-summary/study-summary.component';
import { StudyCardComponent } from './study-card/study-card.component';
import { StudiesRoutingModule } from './studies-routing.module';
import { StudyTraitsComponent } from './study-traits/study-traits.component';
import { StudyObservationsComponent } from './study-observations/study-observations.component';
import { StudyEntriesComponent } from './study-entries/study-entries.component';
import { StudyExperimentsComponent } from './study-experiments/study-experiments.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule.forRoot(),
		AgGridModule.withComponents([]),
    StudiesRoutingModule,
    FlexLayoutModule.forRoot(),
    ServiceModule
  ],
  declarations: [
    StudiesComponent,
    StudyWizardComponent,
    StudyDetailComponent,
	  StudyHomeComponent,
	  StudyListComponent,
	  StudySummaryComponent,
	  StudyCardComponent,
	  StudyTraitsComponent,
	  StudyObservationsComponent,
	  StudyEntriesComponent,
	  StudyExperimentsComponent
	],
	exports: [
		StudiesComponent
	]
})
export class StudiesModule { }

