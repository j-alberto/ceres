import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { StudiesComponent } from './studies.component';
import { StudyWizardComponent } from './study-wizard/study-wizard.component';
import { StudyDetailComponent } from './study-detail/study-detail.component';
import { StudyHomeComponent } from './study-home/study-home.component';

const routes: Routes = [
	{
		path: '',
		component: StudiesComponent,
		children: [
			{	
				path: 'register',
				component: StudyWizardComponent
			},{
				path: ':id',
				component: StudyDetailComponent
			},{	
				path: ':id/edit',
				component: StudyWizardComponent
			},{
				path: '',
				component: StudyHomeComponent
			}
		]}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class StudiesRoutingModule{ }