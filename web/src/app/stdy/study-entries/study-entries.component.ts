import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { GridOptions } from 'ag-grid/main';

import { Entry } from '../../mdl/entry';
import { Study } from '../../mdl/study';

@Component({
  selector: 'crs-study-entries',
  templateUrl: './study-entries.component.html',
  styleUrls: ['./study-entries.component.css']
})
export class StudyEntriesComponent implements OnChanges {

  @Input()
  private study:Study;
  private entriesGridOptions:GridOptions;
  private entriesColumns:any[] = [
    { headerName:'Number', field: 'sequence'},
    { headerName:'Creator', field: 'createdOn'},
    { headerName:'Type', field: 'type'},
    { headerName:'Pedigree', field: 'germplasm'},
    { headerName:'Selection History', field: 'germplasmId'},
  ];

  constructor() { 
   this.entriesGridOptions = <GridOptions>{};
	}

  ngOnChanges() {
    if(this.entriesGridOptions.api)
      this.entriesGridOptions.api.setRowData(this.study.entries);
  }

}
