import { Injectable } from '@angular/core';
import { Headers,Http } from '@angular/http';
import { Study } from '../mdl/study';
import { Page } from '../mdl/page';
import {environment} from '../../environments/environment';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class StudyService {

	private studyUrl = environment.serviceHost+'api/study';
	private headers = new Headers({'Content-Type': 'application/json'});
	loading: boolean = false;

  constructor(private http:Http) { }

  getStudies(page:number, size:number): Promise<Page<Study>> {
  	this.loading = true;
  	return this.http.get(`${this.studyUrl}?page=${page}&size=${size}`)
			.toPromise()
			.then(response => {this.loading = false; 
				return response.json() as Page<Study>;
			})
			.catch(this.handleError);
  }

  getStudy(id: number): Promise<Study> {
  	this.loading = true;
  	return this.http.get(`${this.studyUrl}/${id}`)
  		.toPromise()
  		.then(response => {
  			this.loading = false;
  			return response.json();
  	});
  }

  createStudy(study: Study): Promise<Study> {
  	  this.loading = true;
	  return this.http
	    .post(this.studyUrl, JSON.stringify(study), {headers: this.headers})
	    .toPromise()
	    .then(res => {
	    	this.loading = false;
	    	return res.json();
	    })
	    .catch(this.handleError);
	}

  updateStudy(study:Study): Promise<Study> {
  	this.loading = true;
		return this.http.put(`${this.studyUrl}/${study.id}`, JSON.stringify(study),{headers: this.headers})
			.toPromise()
			.then(()=> {
				this.loading = false;
				return study;
			})
			.catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
	  console.error('[ceres] An error occurred', error);
	  this.loading = false;
	  return Promise.reject(error.message || error);
  }

}
