import { Injectable } from '@angular/core';
import { Headers,Http } from '@angular/http';
import { Program } from '../mdl/program';
import {environment} from '../../environments/environment';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ProgramService {

	private programUrl = environment.serviceHost+'api/program';
	private headers = new Headers({'Content-Type': 'application/json'});
	loading: boolean = false;

  constructor(private http:Http) { }

  getPrograms(): Promise<Program[]> {
  	this.loading = true;
  	return this.http.get(this.programUrl)
			.toPromise()
			.then(response => {this.loading = false; 
				return response.json() as Program[];
			})
			.catch(this.handleError);
  }

  getProgram(id: number): Promise<Program> {
  	this.loading = true;
  	return this.http.get(`${this.programUrl}/${id}`)
  		.toPromise()
  		.then(response => {
  			this.loading = false;
  			return response.json();
  	});
  }

  createProgram(program: Program): Promise<Program> {
  	  this.loading = true;
	  return this.http
	    .post(this.programUrl, JSON.stringify(program), {headers: this.headers})
	    .toPromise()
	    .then(res => {
	    	this.loading = false;
	    	return res.json();
	    })
	    .catch(this.handleError);
	}

  updateProgram(program:Program): Promise<Program> {
  	this.loading = true;
		return this.http.put(`${this.programUrl}/${program.id}`, JSON.stringify(program),{headers: this.headers})
			.toPromise()
			.then(()=> {
				this.loading = false;
				return program;
			})
			.catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
	  console.error('[ceres] An error occurred', error);
	  this.loading = false;
	  return Promise.reject(error.message || error);
  }

}
