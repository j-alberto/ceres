import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Program } from '../mdl/program';
import { Status } from '../mdl/status';
import { Study } from '../mdl/study';
import { Page } from '../mdl/page';

export class InMemoryService implements InMemoryDbService{
    createDb(){
        /** Program: NOT PAGED **/
        let program:Program[] = [
            { id: 1, name: 'Bread Wheat', acronym: 'BW' ,externalReference:777,description:'a bred program',
              crop:'Wheat', status: 'pending', createdOn:'2012-01-01 12:30:55', createdByUserId: 2,
              cropId: 2, statusId:1},
            { id: 2, name: 'Triticale', acronym: 'TC' ,externalReference:999,description:'a trit program',
              crop:'Wheat', status: 'pending', createdOn:'2012-01-01 12:32:56', createdByUserId: 2,
              cropId: 2, statusId:1},
           
        ];

        /**Status: NOT PAGED **/
        let status:Status[] = [
            { id: 1, name: 'Pending', description:'not validated or curated' },
            { id: 2, name: 'Active', description:'ready for use' },
            { id: 3, name: 'Blocked', description:'temporary unavailable' },
            { id: 4, name: 'Deleted', description:'not valid anymore' }
        ];

        /**Study: PAGINATED **/
      let study: any[] = [{
        last: true,
        totalElements:2,
        totalPages:1,
        sort:null,
        first:true,
        numberOfElements:2,
        page:0, //not in real response, faked to match initial request
        size:200,//faked to match initial request
        number: 1,
        content: [
          { id: 1, name: 'StudyA-123', abbreviation: 'BW123-US-1', description:'some large description', type: 'Trial', status: status[0].name,
            createdOn: '2016-01-02 12:01:02', createdBy: 99, targetSeason: 'winter-winter', targetMegaEnvironment: 'SA', ownerUserId: 1,
            program: program[0].name, organization: 'organization A', targetClass: '5Loc', checks: 2, totalEntries: 200, experiments: 50,
            starting:'2016-01-02 12:01:02', ending:'2016-01-02 12:01:02',tests:0,checkOffset:0, checkFrequency:0,
            checkInsertions:0, programId: 1, typeId: 1, crop: 'wheat',cropId:1, statusId:1, organizationId: 1,
            targetMegaEnvironmentId: 1, targetSeasonId: 1, targetClassId:1
          },
          { id: 2, name: 'StudyA-123', abbreviation: 'BW123-MX-2', description:'some large description', type: 'Nursery', status: status[0].name,
            createdOn: '2016-01-02 12:01:02', createdBy: 99, targetSeason: 'spring-winter', targetMegaEnvironment: ' EdoMex', ownerUserId: 2,
            program: program[0].name, organization: 'organization A', targetClass: 'F2', checks:2, totalEntries: 50, experiments: 2,
            starting:'2016-01-02 12:01:02', ending:'2016-01-02 12:01:02',tests:0,checkOffset:0, checkFrequency:0,
            checkInsertions:0, programId: 1, typeId: 1, crop: 'wheat',cropId:1, statusId:1, organizationId: 1,
            targetMegaEnvironmentId: 1, targetSeasonId: 1, targetClassId:1
          },
          ]}];

      study[0].content.forEach(stdy => study[0].content.push(stdy));
      study[0].content.forEach(stdy => study[0].content.push(stdy));
      study[0].content.forEach(stdy => study[0].content.push(stdy));
      study[0].content.forEach(stdy => study[0].content.push(stdy));

        /**Crop: NOT PAGED **/
      let crop = [
            { id: 1, code:'W', name: 'Wheat', statusId:1 },
            { id: 2, code:'M', name: 'Maize', statusId: 1 }
        ];

      return {program, status, study, crop};
    }

    /**
      collections from real server with Pagination be declared here. In-memory data must declare
      page & size attribs matching initial request's values. E.g.: Study 
    **/
    private pagedCollections = ['study'];

    responseInterceptor(responseOptions, requestInfo){
      if(responseOptions.body)
    	  responseOptions.body = responseOptions.body.data;

      if(this.pagedCollections.find(col => col==requestInfo.collectionName) && requestInfo.id == undefined)
        responseOptions.body = responseOptions.body[0];
    	
      return responseOptions;
    }
}
