import { Injectable } from '@angular/core';
import { Headers,Http } from '@angular/http';
import { Crop } from '../mdl/crop';
import {environment} from '../../environments/environment';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class CropService {

	private cropUrl = environment.serviceHost+'api/crop';
	private headers = new Headers({'Content-Type': 'application/json'});
	loading: boolean = false;

  constructor(private http:Http) { }

  getCrops(): Promise<Crop[]> {
  	this.loading = true;
  	return this.http.get(this.cropUrl)
			.toPromise()
			.then(response => {this.loading = false; 
				return response.json() as Crop[];
			})
			.catch(this.handleError);
  }

  getCrop(id: number): Promise<Crop> {
  	this.loading = true;
  	return this.http.get(`${this.cropUrl}/${id}`)
  		.toPromise()
  		.then(response => {
  			this.loading = false;
  			return response.json();
  	});
  }

  createCrop(crop: Crop): Promise<Crop> {
  	  this.loading = true;
	  return this.http
	    .post(this.cropUrl, JSON.stringify(crop), {headers: this.headers})
	    .toPromise()
	    .then(res => {
	    	this.loading = false;
	    	return res.json();
	    })
	    .catch(this.handleError);
	}

  updateCrop(crop:Crop): Promise<Crop> {
  	this.loading = true;
		return this.http.put(`${this.cropUrl}/${crop.id}`, JSON.stringify(crop),{headers: this.headers})
			.toPromise()
			.then(()=> {
				this.loading = false;
				return crop;
			})
			.catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
	  console.error('[ceres] An error occurred', error);
	  this.loading = false;
	  return Promise.reject(error.message || error);
  }

}
