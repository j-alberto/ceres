import { Injectable } from '@angular/core';
import { Headers,Http } from '@angular/http';
import { Status } from '../mdl/status';
import {environment} from '../../environments/environment';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class StatusService {

	private statusUrl = environment.serviceHost+'api/status';
	private headers = new Headers({'Content-Type': 'application/json'});
	loading: boolean = false;

  constructor(private http:Http) { }

  getStatuses(): Promise<Status[]> {
  	this.loading = true;
  	return this.http.get(this.statusUrl)
			.toPromise()
			.then(response => {this.loading = false; 
				return response.json() as Status[];
			})
			.catch(this.handleError);
  }

  getStatus(id: number): Promise<Status> {
  	this.loading = true;
  	return this.http.get(`${this.statusUrl}/${id}`)
  		.toPromise()
  		.then(response => {
  			this.loading = false;
  			return response.json();
  	});
  }

  createStatus(status: Status): Promise<Status> {
  	  this.loading = true;
	  return this.http
	    .post(this.statusUrl, JSON.stringify(status), {headers: this.headers})
	    .toPromise()
	    .then(res => {
	    	this.loading = false;
	    	return res.json();
	    })
	    .catch(this.handleError);
	}

  updateStatus(status:Status): Promise<Status> {
  	this.loading = true;
		return this.http.put(`${this.statusUrl}/${status.id}`, JSON.stringify(status),{headers: this.headers})
			.toPromise()
			.then(()=> {
				this.loading = false;
				return status;
			})
			.catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
	  console.error('[ceres] An error occurred', error); // for demo purposes only
	  this.loading = false;
	  return Promise.reject(error.message || error);
  }

}
