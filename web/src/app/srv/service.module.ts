import { NgModule } from '@angular/core';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';

import { InMemoryService } from '../srv/in-memory.service';
import { CropService } from './crop.service';
import { StatusService } from './status.service';
import { ProgramService } from './program.service';
import { StudyService } from './study.service';

@NgModule({
	imports: [
//		InMemoryWebApiModule.forRoot(InMemoryService, {delay: 350}),
	],
	providers: [
  	CropService,
  	StatusService,
  	ProgramService,
  	StudyService
	]
})
export class ServiceModule { }

