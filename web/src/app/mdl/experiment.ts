export class Experiment {
	id: number;
	number: number;
	name: string;
	abbreviation: string;
	plots: number;
	plotOffset: number;
	replicates: number;
	designTypeId: number;
	designType: string;
	seasonId: number;
	season: string;
	georeferenceId: number;
	georeference: string;
	statusId: number;
	status: string;
	expClassId: number;
	expClass: string;
	megaEnvironmentId: number;
	megaEnvironment: string;
	createdOn: Date;
	createdBy: number;
}