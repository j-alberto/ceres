export class Entry {
	id: number;
	sequence: number;
	germplasmId: number;
	germplasm: string;
	typeId: number;
	type: string;
	sourceEntryId: number;
	createdOn: Date;
	createdBy: number;
}