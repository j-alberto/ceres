export class Trait {
	id: number;
	name: string;
	code: string;
	description: string;
	scale: string;
	method: string;
	variable: string;
}