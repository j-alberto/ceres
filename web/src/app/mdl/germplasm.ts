export class Germplasm {
	id:number;
	pedigree: string;
	selHist: string;
	bcid: string;
	origin: string;
	type: string;
}