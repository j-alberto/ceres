export class Crop{
	id: number;
	code: string;
	name: string;
	statusId: number;
	status: string;
}