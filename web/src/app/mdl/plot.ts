import { Entry } from './entry';

export class Plot {
	id: number;
	sequence: number;
	entry: Entry;
	replicate: number;
	x: number;
	y: number;
	totalObservations: number;
}