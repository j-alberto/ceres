export class Program{
	id: number;
	name: string;
	acronym: string;
	externalReference: number;
	description: string;
	crop: string;
	cropId: number;
	status: string;
	statusId:number;
	createdOn: string;
	createdByUserId:number;
}