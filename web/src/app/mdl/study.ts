import { Experiment } from './experiment';
import { Entry } from './entry';

export class Study{
	id:number;
	name:string;
	abbreviation:string;
	description:string;
	starting:string;
	ending:string;
	tests:number;
	checks:number;
	totalEntries:number;
	checkOffset:number;
	checkFrequency:number;
	checkInsertions:number;
	totalExperiments:number;

	program:string;
	programId:number;

	type:string;
	typeId:number;

	ownerUserId:number;

	crop:string;
	cropId:number;
	status:string;
	statusId:number;
	createdOn:string;
	createdBy:number;

	organization:string;
	organizationId:number;

	targetMegaEnvironment:string;
	targetMegaEnvironmentId:number;

	targetSeason:string;
	targetSeasonId:number;
	targetClass:string;
	targetClassId:number;

	experiments:Experiment[];
	entries:Entry[];
}