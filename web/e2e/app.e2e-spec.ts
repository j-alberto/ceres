import { CeresWebPage } from './app.po';

describe('ceres-web App', function() {
  let page: CeresWebPage;

  beforeEach(() => {
    page = new CeresWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
