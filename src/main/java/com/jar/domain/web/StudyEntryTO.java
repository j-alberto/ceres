package com.jar.domain.web;

import java.util.Date;

public class StudyEntryTO {
	private long id;
	private int sequence;
	private long germplasmId;
	private String germplasm;
	private short typeId;
	private String type;
	private Long sourceEntryId;
	private Date createdOn;
	private short createdBy;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public long getGermplasmId() {
		return germplasmId;
	}
	public void setGermplasmId(long germplasmId) {
		this.germplasmId = germplasmId;
	}
	public String getGermplasm() {
		return germplasm;
	}
	public void setGermplasm(String germplasm) {
		this.germplasm = germplasm;
	}
	public short getTypeId() {
		return typeId;
	}
	public void setTypeId(short typeId) {
		this.typeId = typeId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getSourceEntryId() {
		return sourceEntryId;
	}
	public void setSourceEntryId(Long sourceEntryId) {
		this.sourceEntryId = sourceEntryId;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}

}
