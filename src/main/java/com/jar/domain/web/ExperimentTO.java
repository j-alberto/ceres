package com.jar.domain.web;

import java.util.Date;

public class ExperimentTO {
	private int id;
	private short number;
	private String name;
	private String abbreviation;
	private int plots;
	private int plotOffset;
	private short replicates;
	private short designTypeId;
	private String designType;
	private short seasonId;
	private String season;
	private int georeferenceId;
	private String georeference;
	private short statusId;
	private String status;
	private short expClassId;
	private String expClass;
	private short megaEnvironmentId;
	private String megaEnvironment;
	private Date createdOn;
	private short createdBy;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public short getNumber() {
		return number;
	}
	public void setNumber(short number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public int getPlots() {
		return plots;
	}
	public void setPlots(int plots) {
		this.plots = plots;
	}
	public int getPlotOffset() {
		return plotOffset;
	}
	public void setPlotOffset(int plotOffset) {
		this.plotOffset = plotOffset;
	}
	public short getReplicates() {
		return replicates;
	}
	public void setReplicates(short replicates) {
		this.replicates = replicates;
	}
	public short getDesignTypeId() {
		return designTypeId;
	}
	public void setDesignTypeId(short designTypeId) {
		this.designTypeId = designTypeId;
	}
	public String getDesignType() {
		return designType;
	}
	public void setDesignType(String designType) {
		this.designType = designType;
	}
	public short getSeasonId() {
		return seasonId;
	}
	public void setSeasonId(short seasonId) {
		this.seasonId = seasonId;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	public int getGeoreferenceId() {
		return georeferenceId;
	}
	public void setGeoreferenceId(int georeferenceId) {
		this.georeferenceId = georeferenceId;
	}
	public String getGeoreference() {
		return georeference;
	}
	public void setGeoreference(String georeference) {
		this.georeference = georeference;
	}
	public short getStatusId() {
		return statusId;
	}
	public void setStatusId(short statusId) {
		this.statusId = statusId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public short getExpClassId() {
		return expClassId;
	}
	public void setExpClassId(short expClassId) {
		this.expClassId = expClassId;
	}
	public String getExpClass() {
		return expClass;
	}
	public void setExpClass(String expClass) {
		this.expClass = expClass;
	}
	public short getMegaEnvironmentId() {
		return megaEnvironmentId;
	}
	public void setMegaEnvironmentId(short megaEnvironmentId) {
		this.megaEnvironmentId = megaEnvironmentId;
	}
	public String getMegaEnvironment() {
		return megaEnvironment;
	}
	public void setMegaEnvironment(String megaEnvironment) {
		this.megaEnvironment = megaEnvironment;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}
}
