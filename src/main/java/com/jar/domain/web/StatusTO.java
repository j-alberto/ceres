package com.jar.domain.web;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Front end presentation of {@link StatusTO}.
 * 
 */
public class StatusTO {
	private short id;
	@NotBlank
	private String name;
	@NotBlank
	private String description;

	public short getId() {
		return id;
	}

	public void setId(short id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return String.format("{web.Status#%s %s}", id, name);
	}

}
