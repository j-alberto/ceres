package com.jar.domain.web;

public class SubsetTO {

	private int experimentId;
	private String experiment;
	private int plots;
	private String[] variables;
	private int[] variableIds;
	private String[] dataMatrixHeaders;
	private String[][] dataMatrix;
	
	public int getExperimentId() {
		return experimentId;
	}
	public void setExperimentId(int experimentId) {
		this.experimentId = experimentId;
	}
	public String getExperiment() {
		return experiment;
	}
	public void setExperiment(String experiment) {
		this.experiment = experiment;
	}
	public int getPlots() {
		return plots;
	}
	public void setPlots(int plots) {
		this.plots = plots;
	}
	public String[] getVariables() {
		return variables;
	}
	public void setVariables(String[] variables) {
		this.variables = variables;
	}
	public int[] getVariableIds() {
		return variableIds;
	}
	public void setVariableIds(int[] variableIds) {
		this.variableIds = variableIds;
	}
	public String[] getDataMatrixHeaders() {
		return dataMatrixHeaders;
	}
	public void setDataMatrixHeaders(String[] dataMatrixHeaders) {
		this.dataMatrixHeaders = dataMatrixHeaders;
	}
	public String[][] getDataMatrix() {
		return dataMatrix;
	}
	public void setDataMatrix(String[][] dataMatrix) {
		this.dataMatrix = dataMatrix;
	}
}
