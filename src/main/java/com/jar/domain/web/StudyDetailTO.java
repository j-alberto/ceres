package com.jar.domain.web;

import java.util.Set;

public class StudyDetailTO extends StudyTO {

	private Set<StudyEntryTO> entries;
	private Set<ExperimentTO> experiments;
	
	public Set<StudyEntryTO> getEntries() {
		return entries;
	}
	public void setEntries(Set<StudyEntryTO> entries) {
		this.entries = entries;
	}
	public Set<ExperimentTO> getExperiments() {
		return experiments;
	}
	public void setExperiments(Set<ExperimentTO> experiments) {
		this.experiments = experiments;
	}
}
