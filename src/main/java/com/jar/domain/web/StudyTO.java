package com.jar.domain.web;

import java.util.Date;

public class StudyTO {
	private int id;
	private String name;
	private String abbreviation;
	private	String description;
	private	Date starting;
	private	Date ending;
	private	int tests;
	private	int checks;
	private	int totalEntries;
	private	int checkOffset;
	private	int checkFrequency;
	private	short checkInsertions;
	private	short totalExperiments;
	
	private String program;
	private short programId;
	
	private String type;
	private short typeId;
	
	private Short ownerUserId;
	
	private String crop;
	private short cropId;
	private String status;
	private short statusId;
	private Date createdOn;
	private short createdBy;
	
	private String organization;
	private short organizationId;
	
	private String targetMegaEnvironment;
	private short targetMegaEnvironmentId;
	
	private String targetSeason;
	private short targetSeasonId;
	private String targetClass;
	private short targetClassId;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getStarting() {
		return starting;
	}
	public void setStarting(Date starting) {
		this.starting = starting;
	}
	public Date getEnding() {
		return ending;
	}
	public void setEnding(Date ending) {
		this.ending = ending;
	}
	public int getTests() {
		return tests;
	}
	public void setTests(int tests) {
		this.tests = tests;
	}
	public int getChecks() {
		return checks;
	}
	public void setChecks(int checks) {
		this.checks = checks;
	}
	public int getTotalEntries() {
		return totalEntries;
	}
	public void setTotalEntries(int totalEntries) {
		this.totalEntries = totalEntries;
	}
	public int getCheckOffset() {
		return checkOffset;
	}
	public void setCheckOffset(int checkOffset) {
		this.checkOffset = checkOffset;
	}
	public int getCheckFrequency() {
		return checkFrequency;
	}
	public void setCheckFrequency(int checkFrequency) {
		this.checkFrequency = checkFrequency;
	}
	public short getCheckInsertions() {
		return checkInsertions;
	}
	public void setCheckInsertions(short checkInsertions) {
		this.checkInsertions = checkInsertions;
	}
	public short getTotalExperiments() {
		return totalExperiments;
	}
	public void setTotalExperiments(short totalExperiments) {
		this.totalExperiments = totalExperiments;
	}
	public String getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	public short getProgramId() {
		return programId;
	}
	public void setProgramId(short programId) {
		this.programId = programId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public short getTypeId() {
		return typeId;
	}
	public void setTypeId(short typeId) {
		this.typeId = typeId;
	}
	public Short getOwnerUserId() {
		return ownerUserId;
	}
	public void setOwnerUserId(Short ownerUserId) {
		this.ownerUserId = ownerUserId;
	}
	public String getCrop() {
		return crop;
	}
	public void setCrop(String crop) {
		this.crop = crop;
	}
	public short getCropId() {
		return cropId;
	}
	public void setCropId(short cropId) {
		this.cropId = cropId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public short getStatusId() {
		return statusId;
	}
	public void setStatusId(short statusId) {
		this.statusId = statusId;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	public short getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(short organizationId) {
		this.organizationId = organizationId;
	}
	public String getTargetMegaEnvironment() {
		return targetMegaEnvironment;
	}
	public void setTargetMegaEnvironment(String targetMegaEnvironment) {
		this.targetMegaEnvironment = targetMegaEnvironment;
	}
	public short getTargetMegaEnvironmentId() {
		return targetMegaEnvironmentId;
	}
	public void setTargetMegaEnvironmentId(short targetMegaEnvironmentId) {
		this.targetMegaEnvironmentId = targetMegaEnvironmentId;
	}
	public String getTargetSeason() {
		return targetSeason;
	}
	public void setTargetSeason(String targetSeason) {
		this.targetSeason = targetSeason;
	}
	public short getTargetSeasonId() {
		return targetSeasonId;
	}
	public void setTargetSeasonId(short targetSeasonId) {
		this.targetSeasonId = targetSeasonId;
	}
	public String getTargetClass() {
		return targetClass;
	}
	public void setTargetClass(String targetClass) {
		this.targetClass = targetClass;
	}
	public short getTargetClassId() {
		return targetClassId;
	}
	public void setTargetClassId(short targetClassId) {
		this.targetClassId = targetClassId;
	}

}
