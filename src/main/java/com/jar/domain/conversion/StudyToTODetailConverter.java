package com.jar.domain.conversion;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.Study;
import com.jar.domain.web.StudyDetailTO;

@Component
public class StudyToTODetailConverter implements Converter<Study, StudyDetailTO> {

	private ExperimentToTOConverter experimentConverter;
	private StudyEntryToTOConverter studyEntryConverter;
	private StudyToTOConverter studyConverter;
	
	@Autowired
	public StudyToTODetailConverter(ExperimentToTOConverter experimentConverter,
			StudyEntryToTOConverter studyEntryConverter,
			StudyToTOConverter studyConverter) {
		super();
		this.experimentConverter = experimentConverter;
		this.studyEntryConverter = studyEntryConverter;
		this.studyConverter = studyConverter;
	}

	@Override
	public StudyDetailTO convert(Study source) {
		StudyDetailTO target = (StudyDetailTO)studyConverter.convert(source);
		
		target.setExperiments( source.getExperiments().stream().
				map(exp -> experimentConverter.convert(exp))
				.collect(Collectors.toSet()));
		
		target.setEntries(source.getEntries().stream().
				map(entry -> studyEntryConverter.convert(entry))
				.collect(Collectors.toSet()));
		
		return target;
	}
}
