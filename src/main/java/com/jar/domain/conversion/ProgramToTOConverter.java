package com.jar.domain.conversion;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.Program;
import com.jar.domain.web.ProgramTO;

@Component
public class ProgramToTOConverter implements Converter<Program, ProgramTO> {

	@Override
	public ProgramTO convert(Program source) {
		ProgramTO target = new ProgramTO();
		
		target.setId(source.getId());
		target.setName(source.getName());
		target.setDescription(source.getDescription());
		target.setAcronym(source.getAcronym());
		target.setCreatedOn(source.getCreatedOn());
		target.setCrop( String.valueOf(source.getCropId()) );
		target.setCropId(source.getCropId());
		target.setExternalReference(source.getExternalReference());
		target.setStatusId(source.getStatus().getId());
		target.setStatus(source.getStatus().getName());
		target.setCreatedBy(source.getCreatedBy());
		
		return target;
	}


}
