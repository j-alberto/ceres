package com.jar.domain.conversion;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.DesignType;
import com.jar.domain.base.Experiment;
import com.jar.domain.base.Georeference;
import com.jar.domain.base.MegaEnvironment;
import com.jar.domain.base.Season;
import com.jar.domain.base.Status;
import com.jar.domain.base.StudyClass;
import com.jar.domain.web.ExperimentTO;

@Component
public class TOtoExperimentConverter implements Converter<ExperimentTO, Experiment>{

	@Override
	public Experiment convert(ExperimentTO source) {
		Experiment target = new Experiment();
		
		target.setAbbreviation(source.getAbbreviation());
		target.setCreatedBy(source.getCreatedBy());
		target.setCreatedOn(source.getCreatedOn());
		target.setDesignType(new DesignType(source.getDesignTypeId()));
		target.setExpClass(new StudyClass(source.getExpClassId()));
		target.setGeoreference(new Georeference(source.getGeoreferenceId()));
		target.setId(source.getId());
		target.setMegaEnvironment(new MegaEnvironment(source.getMegaEnvironmentId()));
		target.setName(source.getName());
		target.setNumber(source.getNumber());
		target.setPlotOffset(source.getPlotOffset());
		target.setPlots(source.getPlots());
		target.setReplicates(source.getReplicates());
		target.setSeason(new Season(source.getSeasonId()));
		target.setStatus(new Status(source.getStatusId()));
		
		return target;
	}

}
