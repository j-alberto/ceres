package com.jar.domain.conversion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

@Component
public class ConverterImpl implements Converter {
	
	private ConversionService conversionService;
	
	@Autowired
	public ConverterImpl(ConversionService conversionService) {
		super();
		this.conversionService = conversionService;
	}

	/* (non-Javadoc)
	 * @see com.jar.domain.conversion.Converter#convert(java.util.List, java.lang.Class, java.lang.Class)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <S,T> List<T> convert(List<S> sourceList, Class<S> sourceType, Class<T> targetType) {
	    return (List<T>) conversionService.convert(
	            sourceList,
	            TypeDescriptor.collection(List.class,
	                    TypeDescriptor.valueOf(sourceType)),
	            TypeDescriptor.collection(List.class,
	                    TypeDescriptor.valueOf(targetType)));
    }
	
	/* (non-Javadoc)
	 * @see com.jar.domain.conversion.Converter#convert(java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T> T convert(Object source, Class<T> targetType) {
		return conversionService.convert(source, targetType);
	}
}
