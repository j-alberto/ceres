package com.jar.domain.conversion;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.Crop;
import com.jar.domain.web.CropTO;

@Component
public class CropToTOConverter implements Converter<Crop, CropTO>{

	@Override
	public CropTO convert(Crop source) {
		CropTO target = new CropTO();
		target.setCode(source.getCode());
		target.setId(source.getId());
		target.setName(source.getName());
		target.setStatus(source.getStatus().getName());
		target.setStatusId(source.getStatus().getId());
		
		return target;
	}

}
