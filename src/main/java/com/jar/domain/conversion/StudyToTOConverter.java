package com.jar.domain.conversion;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.Study;
import com.jar.domain.web.StudyDetailTO;
import com.jar.domain.web.StudyTO;

@Component
public class StudyToTOConverter  implements Converter<Study, StudyTO> {

	@Override
	public StudyTO convert(Study source) {
		StudyTO target = new StudyDetailTO();
		target.setId(source.getId());
		target.setName(source.getName());
		target.setAbbreviation(source.getAbbreviation());
		target.setDescription(source.getDescription());
		target.setStarting(source.getStarting());
		target.setEnding(source.getEnding());
		target.setTests(source.getTests());
		target.setChecks(source.getChecks());
		target.setTotalEntries(source.getTotalEntries());
		target.setCheckOffset(source.getCheckOffset());
		target.setCheckFrequency(source.getCheckFrequency());
		target.setCheckInsertions(source.getCheckInsertions());
		target.setTotalExperiments(source.getTotalExperiments());
		target.setOwnerUserId(source.getOwnerUserId());
		target.setCreatedOn(source.getCreatedOn());
		target.setCreatedBy(source.getCreatedBy());
		
		target.setProgram(source.getProgram().getName());
		target.setProgramId(source.getProgram().getId());
		target.setType(source.getType().getName());
		target.setTypeId(source.getType().getId());
		target.setCrop(source.getCrop().getName());
		target.setCropId(source.getCrop().getId());
		target.setStatus(source.getStatus().getName());
		target.setStatusId(source.getStatus().getId());
		
		target.setOrganization(source.getOrganization().getName());
		target.setOrganizationId(source.getOrganization().getId());
		target.setTargetSeason(source.getTargetSeason().getName());
		target.setTargetSeasonId(source.getTargetSeason().getId());
		target.setTargetMegaEnvironment(source.getTargetMegaEnvironment().getName());
		target.setTargetMegaEnvironmentId(source.getTargetMegaEnvironment().getId());
		target.setTargetClass(source.getTargetClass().getName());
		target.setTargetClassId(source.getTargetClass().getId());
		
		
		return target;
	}
	
}