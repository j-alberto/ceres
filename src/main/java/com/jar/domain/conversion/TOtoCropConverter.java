package com.jar.domain.conversion;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.Crop;
import com.jar.domain.base.Status;
import com.jar.domain.web.CropTO;

@Component
public class TOtoCropConverter implements Converter<CropTO, Crop>{

	@Override
	public Crop convert(CropTO source) {
		Crop target = new Crop();
		target.setCode(source.getCode());
		target.setId(source.getId());
		target.setName(source.getName());
		target.setStatus( new Status(source.getStatusId()));
		
		return target;
	}

}
