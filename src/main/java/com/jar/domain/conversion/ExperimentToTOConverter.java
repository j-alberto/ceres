package com.jar.domain.conversion;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.Experiment;
import com.jar.domain.web.ExperimentTO;

@Component
public class ExperimentToTOConverter implements Converter<Experiment, ExperimentTO>{

	@Override
	public ExperimentTO convert(Experiment source) {
		ExperimentTO target = new ExperimentTO();
		
		target.setAbbreviation(source.getAbbreviation());
		target.setCreatedBy(source.getCreatedBy());
		target.setCreatedOn(source.getCreatedOn());
		target.setDesignType(source.getDesignType().getName());
		target.setDesignTypeId(source.getDesignType().getId());
		target.setExpClass(source.getExpClass().getName());
		target.setExpClassId(source.getExpClass().getId());
		target.setGeoreference(source.getGeoreference().getName());
		target.setGeoreferenceId(source.getGeoreference().getId());
		target.setId(source.getId());
		target.setMegaEnvironment(source.getMegaEnvironment().getName());
		target.setMegaEnvironmentId(source.getMegaEnvironment().getId());
		target.setName(source.getName());
		target.setNumber(source.getNumber());
		target.setPlotOffset(source.getPlotOffset());
		target.setPlots(source.getPlots());
		target.setReplicates(source.getReplicates());
		target.setSeason(source.getSeason().getName());
		target.setSeasonId(source.getSeason().getId());
		target.setStatus(source.getStatus().getName());
		target.setStatusId(source.getStatus().getId());
		
		return target;
	}

}
