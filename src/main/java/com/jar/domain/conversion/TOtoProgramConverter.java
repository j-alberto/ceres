package com.jar.domain.conversion;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.Program;
import com.jar.domain.base.Status;
import com.jar.domain.web.ProgramTO;

@Component
public class TOtoProgramConverter implements Converter<ProgramTO, Program> {

	@Override
	public Program convert(ProgramTO source) {
		Program target = new Program();
		
		target.setId(source.getId());
		target.setName(source.getName());
		target.setDescription(source.getDescription());
		target.setAcronym(source.getAcronym());
		target.setCreatedOn(source.getCreatedOn());
		target.setCropId(source.getCropId());
		target.setExternalReference(source.getExternalReference());
		target.setStatus(new Status(source.getStatusId()));
		
		return target;
	}
}
