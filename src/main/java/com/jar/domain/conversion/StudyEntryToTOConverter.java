package com.jar.domain.conversion;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.StudyEntry;
import com.jar.domain.web.StudyEntryTO;

@Component
public class StudyEntryToTOConverter implements Converter<StudyEntry, StudyEntryTO>{

	@Override
	public StudyEntryTO convert(StudyEntry source) {
		StudyEntryTO target = new StudyEntryTO();
		
		target.setCreatedBy(source.getCreatedBy());
		target.setCreatedOn(source.getCreatedOn());
		target.setGermplasm("GID: "+source.getGermplasm().getGid());
		target.setGermplasmId(source.getGermplasm().getId());
		target.setId(source.getId());
		target.setSequence(source.getSequence());
		target.setSourceEntryId(source.getSourceEntryId());
		target.setType(source.getType().getName());
		target.setTypeId(source.getType().getId());
		
		return target;
	}

}
