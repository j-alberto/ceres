package com.jar.domain.conversion;

import com.jar.domain.base.Experiment;
import com.jar.domain.base.Plot;
import com.jar.domain.base.Variable;
import com.jar.domain.repository.PlotRepository;
import com.jar.domain.web.SubsetTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

/**
 * Created by jarojas on 9/03/17.
 */
public class ExperimentToSubsetTOConverter implements Converter<Experiment, SubsetTO>{

    private PlotRepository plotRepo;

    @Autowired
    public ExperimentToSubsetTOConverter(PlotRepository plotRepo) {
        this.plotRepo = plotRepo;
    }

    @Override
    public SubsetTO convert(Experiment source) {
        SubsetTO target = new SubsetTO();
            target.setExperimentId(source.getId());
            target.setExperiment(source.getName());
            target.setPlots(source.getPlots());

        initVariables(source, target);
        initPlots(target);

        return target;
    }


    private  void initVariables(Experiment experiment, SubsetTO subset) {
        Set<Variable> variables = experiment.getStudy().getVariables();
        int[] variableIds = new int[variables.size()];
        String[] variableNames = new String[variables.size()];

        int i=0;
        for (Variable variable : variables) {
            variableIds[i] = variable.getId();
            variableNames[i] = ""+variable.getProperty().getId(); //TODO must be the name of the variable
        }

        subset.setVariables(variableNames);
        subset.setVariableIds(variableIds);

    }

    private void initPlots(SubsetTO subset) {
        Set<Plot> plots = Optional.ofNullable( plotRepo.findByExperimentId(subset.getExperimentId()))
                .orElseGet(()-> Collections.emptySet());


        String[] dataMatrixHeaders = new String[subset.getVariableIds().length+6];
        dataMatrixHeaders[0]="experimentId";
        dataMatrixHeaders[1]="entryId";
        dataMatrixHeaders[2]="entry";
        dataMatrixHeaders[3]="replicate";
        dataMatrixHeaders[4]="plotId";
        dataMatrixHeaders[5]="plot";

        String[][] dataMatrix = new String[dataMatrixHeaders.length][subset.getPlots()];

        int n = 0;
        for (int i = 6; i < 6 + subset.getVariableIds().length; i++) {
            dataMatrixHeaders[i] = subset.getVariables()[n];
            n++;
        }

        for (Plot plot : plots) {
            int plotIndex = plot.getNumber()-1;
            dataMatrix[0][plotIndex] = ""+plot.getExperimentId();
            dataMatrix[1][plotIndex] = ""+plot.getStudyEntryId();
            dataMatrix[2][plotIndex] = ""+plot.getStudyEntryId();
            dataMatrix[3][plotIndex] = ""+plot.getReplicate();
            dataMatrix[4][plotIndex] = ""+plot.getId();
            dataMatrix[5][plotIndex] = ""+plot.getNumber();

            n = 6;
            for (int varId : subset.getVariableIds()) {
                dataMatrix[n++][plotIndex] = "N/O: "+varId;
                //TODO iterate over plot dataset;
            }
        }

        subset.setPlots(plots.size());
        subset.setDataMatrixHeaders(dataMatrixHeaders);
        subset.setDataMatrix(dataMatrix);
    }
}
