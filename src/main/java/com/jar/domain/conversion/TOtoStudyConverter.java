package com.jar.domain.conversion;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.Status;
import com.jar.domain.base.Study;
import com.jar.domain.web.StudyTO;

@Component
public class TOtoStudyConverter implements Converter<StudyTO, Study> {

	@Override
	public Study convert(StudyTO source) {
		Study target = new Study();
		target.setId(source.getId());
		target.setName(source.getName());
		target.setStatus(new Status()); //TODO contructor with id
		
		return target;
	}

}
