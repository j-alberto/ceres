package com.jar.domain.conversion;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.jar.domain.base.Status;
import com.jar.domain.web.StatusTO;

@Component
public class TOtoStatusConverter implements Converter<StatusTO, Status> {

	@Override
	public Status convert(StatusTO source) {
		Status target = new Status();
		
		target.setId(source.getId());
		target.setName(source.getName());
		target.setDescription(source.getDescription());
		
		return target;
	}


}
