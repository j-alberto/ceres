package com.jar.domain.conversion;

import java.util.List;

public interface Converter {

	<S, T> List<T> convert(List<S> sourceList, Class<S> sourceType, Class<T> targetType);

	<T> T convert(Object source, Class<T> targetType);

}