package com.jar.domain.base;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
abstract class AbstractDetail {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false) private
	short id;
	@Column(nullable=false, length=50)private
	String value;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="customFieldId", nullable=false)private
	CustomField customField;
	
	public short getId() {
		return id;
	}
	public void setId(short id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public CustomField getCustomField() {
		return customField;
	}
	public void setCustomField(CustomField customField) {
		this.customField = customField;
	}
	
}
