package com.jar.domain.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="georeferenceType", schema="cm")
public class GeoreferenceType extends AbstractType{

	@Column(nullable=false) 
	private Date createdOn;
	@Column(nullable=false)
	private short createdBy;
	
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdByUserId) {
		this.createdBy = createdByUserId;
	}	
}
