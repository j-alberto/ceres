package com.jar.domain.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="germplasm", schema="gp")
public class Germplasm {
	@Id
	@Column(nullable=false) 
	@GeneratedValue(strategy=GenerationType.IDENTITY) private long id;
	@Column(nullable=false) private long gid;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="typeId", nullable=false) private GermplasmType type;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="cropId", nullable=false) private Crop crop;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="breedingMethodId", nullable=false) private BreedingMethod breedingMethod;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="georeferenceId", nullable=false) private Georeference georeference;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="statusId", nullable=false) private Status status;
	@Column private Long speciesId;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="programId", nullable=false) private Program program;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="organizationId", nullable=false) private Organization organization;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="megaEnvironmentId", nullable=false) private MegaEnvironment megaEnvironment;
	@Column(nullable=false) private Date createdOn;
	@Column(nullable=false) private short createdBy;
	@Column private Short biologicalStatusId;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getGid() {
		return gid;
	}
	public void setGid(long gid) {
		this.gid = gid;
	}
	public GermplasmType getType() {
		return type;
	}
	public void setType(GermplasmType type) {
		this.type = type;
	}
	public Crop getCrop() {
		return crop;
	}
	public void setCrop(Crop crop) {
		this.crop = crop;
	}
	public BreedingMethod getBreedingMethod() {
		return breedingMethod;
	}
	public void setBreedingMethod(BreedingMethod breedingMethod) {
		this.breedingMethod = breedingMethod;
	}
	public Georeference getGeoreference() {
		return georeference;
	}
	public void setGeoreference(Georeference georeference) {
		this.georeference = georeference;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Long getSpeciesId() {
		return speciesId;
	}
	public void setSpeciesId(Long speciesId) {
		this.speciesId = speciesId;
	}
	public Program getProgram() {
		return program;
	}
	public void setProgram(Program program) {
		this.program = program;
	}
	public Organization getOrganization() {
		return organization;
	}
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	public MegaEnvironment getMegaEnvironment() {
		return megaEnvironment;
	}
	public void setMegaEnvironment(MegaEnvironment megaEnvironment) {
		this.megaEnvironment = megaEnvironment;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}
	public Short getBiologicalStatusId() {
		return biologicalStatusId;
	}
	public void setBiologicalStatusId(Short biologicalStatusId) {
		this.biologicalStatusId = biologicalStatusId;
	}
}
