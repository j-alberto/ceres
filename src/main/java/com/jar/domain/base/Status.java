package com.jar.domain.base;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Persistent class. Represents the system state of a record in other entities.
 * 
 */
@Entity
@Table(name="status", schema="cm")
public class Status implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false) private
	short id;
	@Column(nullable=false, length=25) private
	String name;
	@Column(nullable=false, length=100)	private
	String description;
	
		public Status() {}
		
		public Status(short id) {
			super();
			this.id = id;
		}

	public short getId() {
		return id;
	}

	public void setId(short id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return String.format("{Status#%s %s}", id, name);
	}

}
