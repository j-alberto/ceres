package com.jar.domain.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="breedingMethodType", schema="cm")
public class BreedingMethodType extends AbstractType{

	@Column(nullable=false, length=4) private
		String code;
	@Column(nullable=false) private
		Date createdOn;
	@Column(nullable=false) private
		short createdBy;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}
}
