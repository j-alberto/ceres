package com.jar.domain.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="class", schema="fb")
public class StudyClass {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false) private short id;
	@Column(nullable=false, length=25)private String name;
	@Column(nullable=false, length=15)private String acronym;
	@Column(nullable=false, length=100)private String description;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="statusId", nullable=false)private Status status;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cropId", nullable=false)private Crop crop;
	@Column(nullable=false) private Date createdOn;
	@Column(nullable=false) private short createdBy;

		public StudyClass() {}
		
		public StudyClass(short id) {
			this.setId(id);
		}

	public short getId() {
		return id;
	}
	public void setId(short id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAcronym() {
		return acronym;
	}
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Crop getCrop() {
		return crop;
	}
	public void setCrop(Crop crop) {
		this.crop = crop;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}
	
	
}
