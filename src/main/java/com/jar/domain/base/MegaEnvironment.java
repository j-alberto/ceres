package com.jar.domain.base;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="megaEnvironment", schema="cm")
public class MegaEnvironment {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false) private
	short id;
	@Column(nullable=false, length=50)private
	String name;
	@Column(nullable=false, length=15)private
	String acronym;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="statusId", nullable=false)private
	Status status;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cropId", nullable=false)private
	Crop crop;
	
		public MegaEnvironment() {}
		
		public MegaEnvironment(short id) {
			this.id = id;
		}
	
	public short getId() {
		return id;
	}
	public void setId(short id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAcronym() {
		return acronym;
	}
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Crop getCrop() {
		return crop;
	}
	public void setCrop(Crop crop) {
		this.crop = crop;
	}
	
}
