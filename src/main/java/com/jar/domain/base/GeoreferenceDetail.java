package com.jar.domain.base;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="georeferenceDetail", schema="cm")
public class GeoreferenceDetail extends AbstractDetail{

	@Column(nullable=false) private
	int georeferenceId;

	public int getGeoreferenceId() {
		return georeferenceId;
	}

	public void setGeoreferenceId(int georeferenceId) {
		this.georeferenceId = georeferenceId;
	}
}
