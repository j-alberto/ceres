package com.jar.domain.base;

import javax.persistence.*;

/**
 * Created by jarojas on 10/03/17.
 */
@Entity
@Table(name = "method", schema = "on")
public class Method {
    @Id
    @Column(nullable=false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
