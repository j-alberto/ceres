package com.jar.domain.base;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

/**
 * Created by jarojas on 10/03/17.
 */
@Entity
@Table(name = "variable", schema = "on")
public class Variable {
    @Id
    @Column(nullable=false)
    @GeneratedValue(strategy= GenerationType.IDENTITY) private int id;
    @ManyToOne @Fetch(FetchMode.JOIN)
    @JoinColumn(name="methodId", nullable=false) private Method method;
    @ManyToOne @Fetch(FetchMode.JOIN)
    @JoinColumn(name="scaleId", nullable=false) private Scale scale;
    @ManyToOne @Fetch(FetchMode.JOIN)
    @JoinColumn(name="propertyId", nullable=false) private Property property;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Scale getScale() {
        return scale;
    }

    public void setScale(Scale scale) {
        this.scale = scale;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }
}
