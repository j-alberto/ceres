package com.jar.domain.base;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by jarojas on 10/03/17.
 */
@Entity
@Table(name = "plot", schema = "fb")
@EntityListeners(AuditingEntityListener.class)
public class Plot {
    @Id
    @Column(nullable=false)
    @GeneratedValue(strategy=GenerationType.IDENTITY) private long id;
    @Column(nullable=false) private long studyEntryId;
    @Column(nullable=false) private int experimentId;
    @Column(nullable=false) private int number;
    @Column(nullable=false) private short replicate;
    @Column private short subBlock;
    @Column private short row;
    @Column private short column;
    @CreatedDate
    @Column(nullable = false) private Date createdOn;
    @CreatedBy
    @Column(nullable = false) private short createdBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStudyEntryId() {
        return studyEntryId;
    }

    public void setStudyEntryId(long studyEntryId) {
        this.studyEntryId = studyEntryId;
    }

    public int getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(int experimentId) {
        this.experimentId = experimentId;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public short getReplicate() {
        return replicate;
    }

    public void setReplicate(short replicate) {
        this.replicate = replicate;
    }

    public short getSubBlock() {
        return subBlock;
    }

    public void setSubBlock(short subBlock) {
        this.subBlock = subBlock;
    }

    public short getRow() {
        return row;
    }

    public void setRow(short row) {
        this.row = row;
    }

    public short getColumn() {
        return column;
    }

    public void setColumn(short column) {
        this.column = column;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public short getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(short createdBy) {
        this.createdBy = createdBy;
    }
}
