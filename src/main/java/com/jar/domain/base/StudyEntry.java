package com.jar.domain.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="studyEntry", schema="fb")
public class StudyEntry {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false) private long id;
	@Column(nullable=false) private int sequence;
	@Column(nullable=false) private int studyId;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="germplasmId", nullable=false) private Germplasm germplasm;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="typeId", nullable=false) private StudyEntryType type;
	@Column private Long sourceEntryId;
	@Column(nullable=false) private Date createdOn;
	@Column(nullable=false) private short createdBy;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getSequence() {
		return sequence;
	}
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}
	public int getStudyId() {
		return studyId;
	}
	public void setStudyId(int studyId) {
		this.studyId = studyId;
	}
	public Germplasm getGermplasm() {
		return germplasm;
	}
	public void setGermplasm(Germplasm germplasm) {
		this.germplasm = germplasm;
	}
	public StudyEntryType getType() {
		return type;
	}
	public void setType(StudyEntryType type) {
		this.type = type;
	}
	public Long getSourceEntryId() {
		return sourceEntryId;
	}
	public void setSourceEntryId(Long sourceEntryId) {
		this.sourceEntryId = sourceEntryId;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}
}
