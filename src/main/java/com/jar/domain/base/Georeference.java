package com.jar.domain.base;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="georeference", schema="cm")
public class Georeference {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)private
		int id;
	@Column(nullable=false, length=8)private
		String code;
	@Column(nullable=false, length=25)private
		String name;
	@Column(nullable=false)private
		short hierarchy;
	@Column()private
		Float latitude;
	@Column()private
		Float longitude;
	@Column()private
		Float elevation;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cropId", nullable=false)private
		Crop crop;
	@ManyToOne()
	@JoinColumn(name="typeId", nullable=false)private
		GeoreferenceType type;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="statusId", nullable=false)private
		Status status;
	@Column()private
		Integer parentGeoreferenceId;
	@Column(nullable=false)private
		Date createdOn;
	@Column(nullable=false)private
		short createdBy;
	@OneToMany(mappedBy="georeferenceId")
	List<GeoreferenceDetail> details;
	
		public Georeference() {}
		
		public Georeference(int id) {
			this.id = id;
		}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public short getHierarchy() {
		return hierarchy;
	}
	public void setHierarchy(short hierarchy) {
		this.hierarchy = hierarchy;
	}
	public Float getLatitude() {
		return latitude;
	}
	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}
	public Float getLongitude() {
		return longitude;
	}
	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}
	public Float getElevation() {
		return elevation;
	}
	public void setElevation(Float elevation) {
		this.elevation = elevation;
	}
	public Crop getCrop() {
		return crop;
	}
	public void setCrop(Crop crop) {
		this.crop = crop;
	}
	public GeoreferenceType getType() {
		return type;
	}
	public void setType(GeoreferenceType type) {
		this.type = type;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Integer getParentGeoreferenceId() {
		return parentGeoreferenceId;
	}
	public void setParentGeoreferenceId(Integer parentGeoreferenceId) {
		this.parentGeoreferenceId = parentGeoreferenceId;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}
	public List<GeoreferenceDetail> getDetails() {
		return details;
	}
	public void setDetails(List<GeoreferenceDetail> details) {
		this.details = details;
	}
}
