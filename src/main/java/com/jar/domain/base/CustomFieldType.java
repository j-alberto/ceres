package com.jar.domain.base;

public enum CustomFieldType {
	B, //boolean
	D, //date
	N, //natural, integers
	R, //real, with decimals
	C //character, text
}
