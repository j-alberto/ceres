package com.jar.domain.base;

import javax.persistence.*;

/**
 * Created by jarojas on 10/03/17.
 */
@Entity
@Table(name = "scale", schema = "on")
public class Scale {
    @Id
    @Column(nullable=false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
