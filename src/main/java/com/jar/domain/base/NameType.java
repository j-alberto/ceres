package com.jar.domain.base;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="nameType", schema="gp")
@AttributeOverride(name = "name", column = @Column(name = "name", nullable=false, length=50))
public class NameType extends AbstractType{

	@Column(nullable=false, length=8) private
		String code;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cropId", nullable=false) private
		Crop crop;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Crop getCrop() {
		return crop;
	}
	public void setCrop(Crop crop) {
		this.crop = crop;
	}
}
