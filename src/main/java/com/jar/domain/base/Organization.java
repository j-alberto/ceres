package com.jar.domain.base;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="organization", schema="cm")
public class Organization {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false) private short id;
	@Column(nullable=false, length=50)private String name;
	@Column(nullable=false, length=4)private String code;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="statusId", nullable=false)private Status status;
	@Column(nullable=false) private Date createdOn;
	@Column(nullable=false) private short createdBy;
	
	public short getId() {
		return id;
	}
	public void setId(short id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}
}
