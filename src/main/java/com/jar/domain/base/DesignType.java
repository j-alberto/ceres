package com.jar.domain.base;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="designType", schema="fb")
public class DesignType extends AbstractType{
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cropId", nullable=false) private
	Crop crop;

		public DesignType() {}
	
		public DesignType(short id) {
			this.setId(id);
		}

	public Crop getCrop() {
		return crop;
	}

	public void setCrop(Crop crop) {
		this.crop = crop;
	}
}
