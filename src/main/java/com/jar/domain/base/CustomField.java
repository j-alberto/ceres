package com.jar.domain.base;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="customField", schema="cm")
public class CustomField {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	short id;
	@Column(nullable=false, length=25) private
	String entity;
	@Column(nullable=false, length=25) private
	String field;
	@Enumerated(EnumType.STRING)
	@Column(nullable=false) private
	CustomFieldType dataType;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="statusId", nullable=false) private
	Status status;
	
	public short getId() {
		return id;
	}
	public void setId(short id) {
		this.id = id;
	}
	public String getEntity() {
		return entity;
	}
	public void setEntity(String entity) {
		this.entity = entity;
	}
	public String getField() {
		return field;
	}
	public void setField(String field) {
		this.field = field;
	}
	public CustomFieldType getDataType() {
		return dataType;
	}
	public void setDataType(CustomFieldType dataType) {
		this.dataType = dataType;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
}
