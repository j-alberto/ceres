package com.jar.domain.base;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="study", schema="fb")
public class Study {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false) private int id;
	@Column(nullable=false, length=50) private String name;
	@Column(nullable=false, length=15) private String abbreviation;
	@Column(nullable=false, length=200) private	String description;
	@Column private Date starting;
	@Column private Date ending;
	@Column(nullable=false) private	int tests;
	@Column(nullable=false) private	int checks;
	@Column(nullable=false) private	int totalEntries;
	@Column(nullable=false) private	int checkOffset;
	@Column(nullable=false) private	int checkFrequency;
	@Column(nullable=false) private	short checkInsertions;
	@Column(name="experiments",nullable=false) private	short totalExperiments;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="programId", nullable=false) private Program program;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="typeId", nullable=false) private StudyType type;
	@Column private Short ownerUserId;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="cropId", nullable=false) private Crop crop;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="statusId", nullable=false) private Status status;
	@Column(nullable=false) private Date createdOn;
	@Column(nullable=false) private short createdBy;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="organizationId", nullable=false) private Organization organization;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="targetMegaEnvironmentId", nullable=false) private	MegaEnvironment targetMegaEnvironment;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="targetSeasonId", nullable=false) private Season targetSeason;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="targetClassId", nullable=false) private StudyClass targetClass;
	@OneToMany(mappedBy="studyId", fetch=FetchType.LAZY) @Fetch(FetchMode.SUBSELECT)
	private Set<StudyEntry> entries;
	@OneToMany(mappedBy="study", fetch=FetchType.LAZY) @Fetch(FetchMode.SUBSELECT)
	private Set<Experiment> experiments;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "variableByStudy", schema = "fb", joinColumns = {	@JoinColumn(name = "studyId", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "variableId",	nullable = false, updatable = false) })
	private Set<Variable> variables;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getStarting() {
		return starting;
	}
	public void setStarting(Date starting) {
		this.starting = starting;
	}
	public Date getEnding() {
		return ending;
	}
	public void setEnding(Date ending) {
		this.ending = ending;
	}
	public int getTests() {
		return tests;
	}
	public void setTests(int tests) {
		this.tests = tests;
	}
	public int getChecks() {
		return checks;
	}
	public void setChecks(int checks) {
		this.checks = checks;
	}
	public int getTotalEntries() {
		return totalEntries;
	}
	public void setTotalEntries(int totalEntries) {
		this.totalEntries = totalEntries;
	}
	public int getCheckOffset() {
		return checkOffset;
	}
	public void setCheckOffset(int checkOffset) {
		this.checkOffset = checkOffset;
	}
	public int getCheckFrequency() {
		return checkFrequency;
	}
	public void setCheckFrequency(int checkFrequency) {
		this.checkFrequency = checkFrequency;
	}
	public short getCheckInsertions() {
		return checkInsertions;
	}
	public void setCheckInsertions(short checkInsertions) {
		this.checkInsertions = checkInsertions;
	}
	public short getTotalExperiments() {
		return totalExperiments;
	}
	public void setTotalExperiments(short totalExperiments) {
		this.totalExperiments = totalExperiments;
	}
	public Program getProgram() {
		return program;
	}
	public void setProgram(Program program) {
		this.program = program;
	}
	public StudyType getType() {
		return type;
	}
	public void setType(StudyType type) {
		this.type = type;
	}
	public Short getOwnerUserId() {
		return ownerUserId;
	}
	public void setOwnerUserId(Short ownerUserId) {
		this.ownerUserId = ownerUserId;
	}
	public Crop getCrop() {
		return crop;
	}
	public void setCrop(Crop crop) {
		this.crop = crop;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}
	public Organization getOrganization() {
		return organization;
	}
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	public MegaEnvironment getTargetMegaEnvironment() {
		return targetMegaEnvironment;
	}
	public void setTargetMegaEnvironment(MegaEnvironment targetMegaEnvironment) {
		this.targetMegaEnvironment = targetMegaEnvironment;
	}
	public Season getTargetSeason() {
		return targetSeason;
	}
	public void setTargetSeason(Season targetSeason) {
		this.targetSeason = targetSeason;
	}
	public StudyClass getTargetClass() {
		return targetClass;
	}
	public void setTargetClass(StudyClass targetClass) {
		this.targetClass = targetClass;
	}
	public Set<StudyEntry> getEntries() {
		return entries;
	}
	public void setEntries(Set<StudyEntry> entries) {
		this.entries = entries;
	}
	public void setExperiments(Set<Experiment> experiments) {
		this.experiments = experiments;
	}
	public Set<Experiment> getExperiments() {
		return experiments;
	}

	public Set<Variable> getVariables() {
		return variables;
	}

	public void setVariables(Set<Variable> variables) {
		this.variables = variables;
	}
}
