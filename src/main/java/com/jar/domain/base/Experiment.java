package com.jar.domain.base;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="experiment", schema="fb")
public class Experiment {

	@Id
	@Column(nullable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY) private int id;
	@ManyToOne(fetch=FetchType.LAZY) @Fetch(FetchMode.SELECT)
	@JoinColumn(name="studyId", nullable=false) private Study study;
	@Column(nullable=false) private short number;
	@Column(nullable=false, length=50) private String name;
	@Column(nullable=false, length=15) private String abbreviation;
	@Column(nullable=false) private int plots;
	@Column(nullable=false) private int plotOffset;
	@Column(nullable=false) private short replicates;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="designTypeId", nullable=false) private DesignType designType;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="seasonId", nullable=false) private Season season;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="georeferenceId", nullable=false) private Georeference georeference;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="statusId", nullable=false) private Status status;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="classId", nullable=false) private StudyClass expClass;
	@ManyToOne @Fetch(FetchMode.JOIN)
	@JoinColumn(name="megaEnvironmentId", nullable=false) private MegaEnvironment megaEnvironment;
	@Column(nullable=false) private Date createdOn;
	@Column(nullable=false) private short createdBy;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Study getStudy() {
		return study;
	}
	public void setStudy(Study study) {
		this.study = study;
	}
	public short getNumber() {
		return number;
	}
	public void setNumber(short number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public int getPlots() {
		return plots;
	}
	public void setPlots(int plots) {
		this.plots = plots;
	}
	public int getPlotOffset() {
		return plotOffset;
	}
	public void setPlotOffset(int plotOffset) {
		this.plotOffset = plotOffset;
	}
	public short getReplicates() {
		return replicates;
	}
	public void setReplicates(short replicates) {
		this.replicates = replicates;
	}
	public DesignType getDesignType() {
		return designType;
	}
	public void setDesignType(DesignType designType) {
		this.designType = designType;
	}
	public Season getSeason() {
		return season;
	}
	public void setSeason(Season season) {
		this.season = season;
	}
	public Georeference getGeoreference() {
		return georeference;
	}
	public void setGeoreference(Georeference georeference) {
		this.georeference = georeference;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public StudyClass getExpClass() {
		return expClass;
	}
	public void setExpClass(StudyClass expClass) {
		this.expClass = expClass;
	}
	public MegaEnvironment getMegaEnvironment() {
		return megaEnvironment;
	}
	public void setMegaEnvironment(MegaEnvironment megaEnvironment) {
		this.megaEnvironment = megaEnvironment;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public short getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(short createdBy) {
		this.createdBy = createdBy;
	}
}
