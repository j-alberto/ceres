package com.jar.domain.repository;

import com.jar.domain.base.Plot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by jarojas on 10/03/17.
 */
public interface PlotRepository extends JpaRepository<Plot, Long> {

    Set<Plot> findByExperimentId(int experimentId);
}
