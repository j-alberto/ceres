package com.jar.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jar.domain.base.Crop;

public interface CropRepository extends JpaRepository<Crop, Short> {

}
