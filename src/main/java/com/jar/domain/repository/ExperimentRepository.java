package com.jar.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jar.domain.base.Experiment;

public interface ExperimentRepository extends JpaRepository<Experiment, Integer> {

	List<Experiment> findByStudyId(int studyId);
}
