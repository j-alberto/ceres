package com.jar.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jar.domain.base.Study;

public interface StudyRepository extends JpaRepository<Study, Integer> {

}
