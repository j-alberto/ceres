package com.jar.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jar.domain.base.Status;

public interface StatusRepository extends JpaRepository<Status, Short>{

}
