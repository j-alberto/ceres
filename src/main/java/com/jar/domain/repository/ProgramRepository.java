package com.jar.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jar.domain.base.Program;

public interface ProgramRepository extends JpaRepository<Program, Short>{

}
