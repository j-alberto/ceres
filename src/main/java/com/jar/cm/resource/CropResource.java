package com.jar.cm.resource;

import java.util.List;

import com.jar.domain.web.CropTO;

public interface CropResource {

	/**
	 * Retrieves all {@link CropTO crops} available
	 * @return a List with all crops
	 */
	List<CropTO> findAll();

	/**
	 * Retrieves a {@link CropTO} with matching id
	 * @param id of the crop
	 * @return a crop, or null if not found
	 */
	CropTO findOne(short id);
	
	/**
	 * Saves a new instance of {@link CropTO}
	 * @param crop to save
	 * @return the saved crop with id property set
	 */
	CropTO saveNew(CropTO crop);

	/**
	 * Updates an existing instance of {@link CropTO}
	 * @param crop to update
	 * @return the saved crop with id property set
	 */
	CropTO update(CropTO crop, short id);

}