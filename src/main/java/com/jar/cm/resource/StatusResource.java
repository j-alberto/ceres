package com.jar.cm.resource;

import java.util.List;

import com.jar.domain.web.StatusTO;

public interface StatusResource {
	/**
	 * Retrieves all {@link StatusTO statuses} available
	 * @return a List of Status
	 */
	List<StatusTO> findAll();

	/**
	 * Retrieves a {@link StatusTO} with matching id
	 * @param id of the status
	 * @return a Status, or null if not found
	 */
	StatusTO findOne(short id);
	
	/**
	 * Saves a new instance of {@link StatusTO}
	 * @param status to save
	 * @return the saved status with id property set
	 */
	StatusTO saveNew(StatusTO status);
}
