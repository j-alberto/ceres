package com.jar.cm.resource.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.jar.domain.web.CropTO;

@Component
public class CropCodeValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return CropTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CropTO crop = (CropTO)target;
		
		if( cropCodeIsReserved(crop) ) {
			errors.rejectValue("code","crop.code.used"
					,new Object[] {crop.getCode()}
					,null);
		}
	}
	
	private boolean cropCodeIsReserved(CropTO crop) {
		return crop.getCode() != null && ( 
				crop.getCode().equals("M") ||
				crop.getCode().equals("W"));
	}
	
}