package com.jar.cm.resource;

import java.util.List;

import com.jar.domain.web.ProgramTO;

public interface ProgramResource {

	List<ProgramTO> findAll();

	ProgramTO findOne(short id);
	
	ProgramTO saveNew(ProgramTO program);
	

}