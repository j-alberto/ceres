package com.jar.cm.resource.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jar.cm.resource.StudyResource;
import com.jar.cm.service.StudyService;
import com.jar.domain.web.StudyDetailTO;
import com.jar.domain.web.StudyTO;
import com.jar.domain.web.SubsetTO;

@RestController
@RequestMapping(value="api/study")
class StudyResourceImpl implements StudyResource {

	private StudyService studyService;

	@Autowired
	public StudyResourceImpl(StudyService studyService) {
		super();
		this.studyService = studyService;
	}
	
	/* (non-Javadoc)
	 * @see com.jar.cm.resource.impl.StudyResource#findAll()
	 */
	@Override
	@GetMapping
	public List<StudyTO> findAll() {
		List<StudyTO> studies = studyService.findAll();
		
		return studies;
	}

	/* (non-Javadoc)
	 * @see com.jar.cm.resource.impl.StudyResource#saveNew(com.jar.domain.web.StudyTO)
	 */
	@Override
	@PostMapping
	public StudyTO saveNew(@RequestBody @Validated StudyTO study) {
		return studyService.addNew(study);
	}
	
	@Override
	@GetMapping(params={"page","size"})
	public Page<StudyTO> findAll(@PageableDefault(size=2) Pageable pageable) {
		Page<StudyTO> studyPage = studyService.findAll(pageable);
		
		return studyPage;
	}

	@Override
	@GetMapping(value="/{id}")
	public StudyDetailTO findDetail( @PathVariable int id) {
		StudyDetailTO study = studyService.findOneDetail(id);
		return study;
	}

	@Override
	@GetMapping("/{id}/dataset")
	public List<SubsetTO> findDataset(int studyId) {
		return studyService.findDataset(studyId);
	}

	@Override
	@GetMapping("/subset/{id}")
	public SubsetTO findSubset(int experimentId) {
		return studyService.findSubset(experimentId);
	}
}
