package com.jar.cm.resource.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jar.cm.resource.CropResource;
import com.jar.cm.resource.validation.CropCodeValidator;
import com.jar.cm.service.CropService;
import com.jar.domain.web.CropTO;

@RestController
@RequestMapping(value="api/crop")
public class CropResourceImpl implements CropResource {

	private CropService cropService;
	private CropCodeValidator cropCodeValidator;
	
		@Autowired
		public CropResourceImpl(CropService cropService, CropCodeValidator cropCodeValidator) {
			super();
			this.cropService = cropService;
			this.cropCodeValidator = cropCodeValidator;
		}
	
	@InitBinder
	private void customizeDataBinder(WebDataBinder webDataBinder) {
		webDataBinder.addValidators(cropCodeValidator);
	}

	@Override
	@GetMapping
	public List<CropTO> findAll() {
		return cropService.findAll();
	}

	@Override
	@GetMapping("/{id}")
	public CropTO findOne(@PathVariable short id) {
		return cropService.findOne(id);
	}

	@Override
	@PostMapping
	public CropTO saveNew(@RequestBody @Validated CropTO crop) {
		return cropService.addNew(crop);
	}
	
	@Override
	@PutMapping("/{id}")
	public CropTO update(@RequestBody @Validated CropTO crop,
			@PathVariable short id) {
		crop.setId(id);
		return cropService.update(crop);
		
	}

}
