package com.jar.cm.resource.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jar.cm.resource.StatusResource;
import com.jar.cm.service.StatusService;
import com.jar.domain.web.StatusTO;

@RestController
@RequestMapping(value="api/status")
class StatusResourceImpl implements StatusResource {
	
	StatusService statusService;
	
	@Autowired
	public StatusResourceImpl(StatusService statusService) {
		super();
		this.statusService = statusService;
	}

	@Override
	@GetMapping
	public List<StatusTO> findAll() {
		List<StatusTO> status = statusService.findAll();
		return status;
	}

	@Override
	@GetMapping(value="/{id}")
	public StatusTO findOne(@PathVariable short id) {
		StatusTO status = statusService.findOne(id);
		return status;
	}

	@Override
	@PostMapping
	public StatusTO saveNew(@RequestBody @Validated StatusTO status) {
		return statusService.addNew(status);
	}

}
