package com.jar.cm.resource.impl;

import java.util.List;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jar.cm.resource.ProgramResource;
import com.jar.cm.service.ProgramService;
import com.jar.domain.web.ProgramTO;

@RestController
@RequestMapping("api/program")
class ProgramResourceImpl implements ProgramResource {

	private final ProgramService programService;

	public ProgramResourceImpl(ProgramService programService) {
		super();
		this.programService = programService;
	}

	/* (non-Javadoc)
	 * @see com.jar.cm.resource.impl.ProgramResource#findAll()
	 */
	@Override
	@GetMapping
	public List<ProgramTO> findAll() {
		List<ProgramTO> program = programService.findAll();		
		return program;
	}

	/* (non-Javadoc)
	 * @see com.jar.cm.resource.impl.ProgramResource#findOne(int)
	 */
	@Override
	@GetMapping(value="/{id}")
	public ProgramTO findOne(@PathVariable short id) {
		ProgramTO program = programService.findOne(id);
		return program;
	}

	@Override
	@PostMapping
	public ProgramTO saveNew(@RequestBody @Validated ProgramTO program) {
		return programService.addNew(program);
	}

}
