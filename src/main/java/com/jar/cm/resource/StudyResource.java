package com.jar.cm.resource;

import com.jar.domain.web.StudyDetailTO;
import com.jar.domain.web.StudyTO;
import com.jar.domain.web.SubsetTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface StudyResource {

	/**
	 * Retrieves all {@link StudyTO studies} available
	 * @return a List of Studies
	 */
	List<StudyTO> findAll();

	/**
	 * Saves a new instance of {@link StudyTO}
	 * @param study to save
	 * @return the saved study with id property set
	 */
	StudyTO saveNew(StudyTO study);

	
	/**
	 * Retrieves a page(subset) of {@link StudyTO studies}
	 * @param pageable to retrieve 
	 * @return a Page with the Studies requested
	 */
	Page<StudyTO> findAll(Pageable pageable);
	
	/**
	 * Retrieves a {@link StudyDetailTO} with matching id
	 * @param id of the study
	 * @return found study with all detail including experiments, entries and observation dataset, or null if not found
	 */
	StudyDetailTO findDetail(int id);
	
	/**
	 * Gets all the plots of all the occurrences for a given study
	 * @param studyId to find its plots
	 * @return The plots of a given study
	 */
	List<SubsetTO> findDataset(int studyId);

	/**
	 * Gets all the plots of the given occurrence
	 * @param experimentId to find its plots
	 * @return The plots of a given experiment
	 */
	SubsetTO findSubset(int experimentId);
}