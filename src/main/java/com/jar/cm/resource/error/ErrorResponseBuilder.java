package com.jar.cm.resource.error;

import java.util.Set;

final class ErrorResponseBuilder {

	private static final ErrorResponseBuilder responseBuilder = new ErrorResponseBuilder();
		
		private ErrorResponseBuilder() {}	
	
	public static final Builder forCode(String code) {
		return responseBuilder.new Builder(code);
	}
	
	class Builder {
		private ErrorResponse response;

		private Builder(String code) {
			super();
			this.response = new ErrorResponse();
			response.setError(code);
		}
		
		public ErrorResponse withMessage(String message) {
			response.setMessages(new String[]{message});
			return response;
		}

		
		public ErrorResponse withMessages(Set<String> messages) {
			response.setMessages(messages.toArray(new String[messages.size()]));
			return response;
		}
	}
}
