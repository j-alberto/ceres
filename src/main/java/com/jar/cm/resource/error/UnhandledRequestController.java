package com.jar.cm.resource.error;

import static com.jar.cm.resource.error.ErrorResponseBuilder.forCode;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.AbstractErrorController;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class UnhandledRequestController extends AbstractErrorController {

	private static final String ERROR_MAPPING = "/error";
	private MessageSource messageSource;
	
		public UnhandledRequestController(ErrorAttributes errorAttributes,
				MessageSource messageSource) {
			super(errorAttributes);
			this.messageSource = messageSource;
		}

	@Override
	public String getErrorPath() {
		return ERROR_MAPPING;
	}
	
	@RequestMapping(value = ERROR_MAPPING)
	public ResponseEntity<ErrorResponse> error(HttpServletRequest request, Locale locale) {
		
		String path = super.getErrorAttributes(request, false).get("path").toString();
		
		String message = messageSource.getMessage(
				"error.missing.resource", new String[]{path}, locale);

		return new ResponseEntity<>(
			forCode("NOT-FOUND-ERROR")
				.withMessage(message),
			HttpStatus.NOT_FOUND);
	}

}
