package com.jar.cm.resource.error;

class ErrorResponse {
	private String error;
	private String[] messages;
	
	public String getError() {
		return error;
	}
	public void setError(String reason) {
		this.error = reason;
	}
	public String[] getMessages() {
		return messages;
	}
	public void setMessages(String[] messages) {
		this.messages = messages;
	}
}
