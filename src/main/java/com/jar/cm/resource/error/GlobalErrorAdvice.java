package com.jar.cm.resource.error;

import static com.jar.cm.resource.error.ErrorResponseBuilder.forCode;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
class GlobalErrorAdvice {

	private static final Logger LOG = LoggerFactory.getLogger(GlobalErrorAdvice.class);

	private MessageSource messageSource;

		@Autowired
		public GlobalErrorAdvice(MessageSource messageSource) {
			super();
			this.messageSource = messageSource;
		}
		
	@ExceptionHandler
	ResponseEntity<ErrorResponse> missingBodyError(
			HttpMessageNotReadableException ex,	Locale locale ) {
		
		LOG.warn(ex.getLocalizedMessage());

		return new ResponseEntity<>(
			forCode("REQUEST-ERROR")
				.withMessage(messageSource.getMessage("error.missing.entity",null, locale))
			, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler
	ResponseEntity<ErrorResponse> fieldValidationError(
			MethodArgumentNotValidException ex,	Locale locale ) {
		
		LOG.warn(ex.getLocalizedMessage());
		
		Set<String> messages = ex.getBindingResult().getFieldErrors().stream()
			.map(error -> 	extractValidationMessage(error, locale))
			.collect(Collectors.toSet());

		return new ResponseEntity<>(
			forCode("VALIDATION-ERROR")
				.withMessages(messages),
			HttpStatus.BAD_REQUEST);
	}
	
	private String extractValidationMessage(FieldError error, Locale locale) {

		return new StringBuilder()
				.append( error.getField())
				.append(": ")
				.append(Optional.ofNullable(error.getDefaultMessage())
						.orElseGet(()->messageSource.getMessage(
								error.getCode(), error.getArguments(), locale)))
				.toString();
	}

	
	@ExceptionHandler
	ResponseEntity<ErrorResponse> generalError(
			Exception ex,	Locale locale ) {
		
		LOG.warn(ex.getLocalizedMessage());

		return new ResponseEntity<>(
			forCode("UNEXPECTED-ERROR")
				.withMessage(ex.getLocalizedMessage())
			, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
