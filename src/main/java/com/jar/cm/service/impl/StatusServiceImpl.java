package com.jar.cm.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jar.cm.service.StatusService;
import com.jar.domain.base.Status;
import com.jar.domain.conversion.Converter;
import com.jar.domain.repository.StatusRepository;
import com.jar.domain.web.StatusTO;

@Service
@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
public class StatusServiceImpl implements StatusService {

	private StatusRepository statusRepo;
	private Converter converter;
	
	
	@Autowired
	public StatusServiceImpl(StatusRepository statusRepo, Converter converter) {
		super();
		this.statusRepo = statusRepo;
		this.converter = converter;
	}

	@Override
	public List<StatusTO> findAll() {
		Optional<List<Status>> optStatuses = Optional.ofNullable(statusRepo.findAll());
		
		return converter.convert(optStatuses.orElse(Collections.emptyList())
					,Status.class, StatusTO.class);
	}

	@Override
	public StatusTO findOne(short id) {
		Optional<Status> optStatus = Optional.ofNullable(statusRepo.findOne(id));

		return converter.convert(optStatus.orElse(null), StatusTO.class);
	}

	@Override
	@Transactional(readOnly=false)
	public StatusTO addNew(StatusTO status) {
		Status
			newStatus =	statusRepo.save(converter.convert(status, Status.class));
		 
		return converter.convert(newStatus, StatusTO.class);
	}

}
