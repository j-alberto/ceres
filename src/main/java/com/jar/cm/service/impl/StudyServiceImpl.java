package com.jar.cm.service.impl;

import java.util.*;

import com.jar.domain.base.Plot;
import com.jar.domain.base.Variable;
import com.jar.domain.repository.PlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jar.cm.service.StudyService;
import com.jar.domain.base.Study;
import com.jar.domain.base.Experiment;
import com.jar.domain.conversion.Converter;
import com.jar.domain.repository.StudyRepository;
import com.jar.domain.repository.ExperimentRepository;
import com.jar.domain.web.StudyDetailTO;
import com.jar.domain.web.StudyTO;
import com.jar.domain.web.SubsetTO;

@Service
@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
public class StudyServiceImpl implements StudyService {

	private StudyRepository studyRepo;
	private Converter converter;
	private ExperimentRepository experimentRepo;

	@Autowired
	public StudyServiceImpl(StudyRepository studyRepo, Converter converter,ExperimentRepository experimentRepo) {
		super();
		this.studyRepo = studyRepo;
		this.converter = converter;
		this.experimentRepo = experimentRepo;
	}

	@Override
	public List<StudyTO> findAll() {
		Optional<List<Study>> optStudies = Optional.ofNullable(studyRepo.findAll());
		
		return converter.convert(optStudies.orElse(Collections.emptyList())
					,Study.class, StudyTO.class);
	}

	@Override
	public StudyTO findOne(int id) {
		Optional<Study> optStudy = Optional.ofNullable(studyRepo.findOne(id));

		return converter.convert(optStudy.orElse(null), StudyTO.class);
	}

	@Override
	@Transactional(readOnly=false)
	public StudyTO addNew(StudyTO study) {
		Study newStudy =	studyRepo.save(converter.convert(study, Study.class));
		 
		return converter.convert(newStudy, StudyTO.class);
	}

	@Override
	public Page<StudyTO> findAll(Pageable pageable) {
		Page<Study> studyPage = studyRepo.findAll(pageable);
		return new PageImpl<>(converter.convert(studyPage.getContent(), Study.class, StudyTO.class)
				, pageable, studyPage.getTotalElements());
	}

	@Override
	public StudyDetailTO findOneDetail(int id) {
		Optional<Study> study = Optional.ofNullable( studyRepo.findOne(id) );
		return converter.convert(study.orElse(null), StudyDetailTO.class);
	}

	@Override
	public List<SubsetTO> findDataset(int studyId) {
		List<Experiment> experiments = experimentRepo.findByStudyId(studyId);
		List<SubsetTO> dataset = new ArrayList<>(experiments.size());

		experiments.forEach(exp -> dataset.add(findSubset(exp.getId())));

		return dataset;
	}

	@Override
	public SubsetTO findSubset(int experimentId) {

		return converter.convert(
				Optional.ofNullable(experimentRepo.findOne(experimentId))
						.orElseGet(() -> null)
				,SubsetTO.class);
	}

}