package com.jar.cm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jar.cm.service.CropService;
import com.jar.domain.base.Crop;
import com.jar.domain.conversion.Converter;
import com.jar.domain.repository.CropRepository;
import com.jar.domain.web.CropTO;

@Service
@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
public class CropServiceImpl implements CropService {

	private CropRepository cropRepo;
	private Converter converter;

	@Autowired
	public CropServiceImpl(CropRepository cropRepo, Converter converter) {
		super();
		this.cropRepo = cropRepo;
		this.converter = converter;
	}

	@Override
	public List<CropTO> findAll() {
		return converter.convert(cropRepo.findAll(), Crop.class, CropTO.class);
	}

	@Override
	public CropTO findOne(short id) {
		return converter.convert(cropRepo.findOne(id), CropTO.class);
	}

	@Override
	@Transactional(readOnly=false)
	public CropTO addNew(CropTO crop) {
		crop.setId((short)0);
		Crop c = cropRepo.save(converter.convert(crop, Crop.class));
		return converter.convert(c, CropTO.class);
	}

	@Override
	@Transactional(readOnly=false)
	public CropTO update(CropTO crop) {
		if(crop.getId() == 0)
			throw new RuntimeException("id not set");
		Crop c = cropRepo.save(converter.convert(crop, Crop.class));
		return converter.convert(c, CropTO.class);
	}

}
