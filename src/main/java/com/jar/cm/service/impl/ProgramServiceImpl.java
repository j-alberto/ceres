package com.jar.cm.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jar.cm.service.ProgramService;
import com.jar.domain.base.Program;
import com.jar.domain.conversion.Converter;
import com.jar.domain.repository.ProgramRepository;
import com.jar.domain.web.ProgramTO;

@Service
@Transactional(propagation=Propagation.REQUIRED, readOnly=true)
public class ProgramServiceImpl implements ProgramService {

	private ProgramRepository programRepo;
	private Converter converter;

	@Autowired
	public ProgramServiceImpl(ProgramRepository programRepo, Converter converter) {
		super();
		this.programRepo = programRepo;
		this.converter = converter;
	}
	
	@Override
	public List<ProgramTO> findAll() {
		Optional<List<Program>> optPrograms = Optional.ofNullable(programRepo.findAll());
		
		return converter.convert(optPrograms.orElse(Collections.emptyList())
					,Program.class, ProgramTO.class);
	}

	@Override
	public ProgramTO findOne(short id) {
		Optional<Program> optStatus = Optional.ofNullable(programRepo.findOne(id));

		return converter.convert(optStatus.orElse(null), ProgramTO.class);
	}

	@Override
	@Transactional(readOnly=false)
	public ProgramTO addNew(ProgramTO program) {
		Program
		newProgram = programRepo.save(converter.convert(program, Program.class));
	 
	return converter.convert(newProgram, ProgramTO.class);
	}

}
