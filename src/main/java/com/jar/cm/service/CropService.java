package com.jar.cm.service;

import java.util.List;

import com.jar.domain.web.CropTO;


public interface CropService {
	/**
	 * Retrieves all {@link CropTO crops} available
	 * @return a List of Crops
	 */
	List<CropTO> findAll();

	/**
	 * Retrieves a {@link CropTO} with matching id
	 * @param id of the crop
	 * @return a crop, or null if not found
	 */
	CropTO findOne(short id);

	/**
	 * Persists a crop
	 * @param crop to save
	 * @return the crop argument with id property set
	 */
	CropTO addNew(CropTO crop);
	
	/**
	 * Persists an existing crop
	 * @param crop to save
	 * @return the updated crop 
	 */
	CropTO update(CropTO crop);
}
