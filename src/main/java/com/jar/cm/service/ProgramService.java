package com.jar.cm.service;

import java.util.List;

import com.jar.domain.web.ProgramTO;


public interface ProgramService {
	/**
	 * Retrieves all {@link ProgramTO statuses} available
	 * @return a List of ProgramTO
	 */
	List<ProgramTO> findAll();

	/**
	 * Retrieves a {@link ProgramTO} with matching id
	 * @param id of the program
	 * @return a ProgramTO, or null if not found
	 */
	ProgramTO findOne(short id);
	
	ProgramTO addNew(ProgramTO program);
}
