package com.jar.cm.service;

import java.util.List;

import com.jar.domain.web.StatusTO;


public interface StatusService {
	/**
	 * Retrieves all {@link StatusTO statuses} available
	 * @return a List of Status
	 */
	List<StatusTO> findAll();

	/**
	 * Retrieves a {@link StatusTO} with matching id
	 * @param id of the status
	 * @return a Status, or null if not found
	 */
	StatusTO findOne(short id);
	
	StatusTO addNew(StatusTO status);
}
