package com.jar.cm.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.jar.domain.web.StudyDetailTO;
import com.jar.domain.web.StudyTO;
import com.jar.domain.web.SubsetTO;

public interface StudyService {

	List<StudyTO> findAll();

	StudyTO findOne(int id);
	
	StudyDetailTO findOneDetail(int id);

	StudyTO addNew(StudyTO study);

	Page<StudyTO> findAll(Pageable pageable);
	
	List<SubsetTO> findDataset(int studyId);
	
	SubsetTO findSubset(int experimentId);
}