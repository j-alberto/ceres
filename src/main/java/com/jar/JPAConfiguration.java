package com.jar;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
public class JPAConfiguration {

	@Bean
	public AuditorAware<Integer> auditorProvider() {
		//TODO implement spring security to retrieve real users
		return () -> 1;
	}
}
