package com.jar.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(method=RequestMethod.GET,
	path={
	"/study/**",
	"/program/**",
	"/status/**",
	"/dashboard/**",
	"/crop/**"})
public class SimpleViewController {
	@GetMapping()
	public String forwardView(){
		return "forward:/index.html";
	}
}
