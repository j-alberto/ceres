package com.jar.aspect;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggerAdvice {

	private static final Logger LOG = LoggerFactory.getLogger(LoggerAdvice.class);

	@Pointcut("within(com.jar.cm.resource.error..*)")
	void isResourceErrorClass() {
		
	}
	
	@Before("@within(org.springframework.web.bind.annotation.RestController) && "
			+ "! isResourceErrorClass()")
	void logRestInputs(JoinPoint joinPoint) {
		logSignatureAndArgs(joinPoint);
	}

	@Before("@within(org.springframework.stereotype.Service)")
	void logServiceInputs(JoinPoint joinPoint) {
		logSignatureAndArgs(joinPoint);
	}
	
	@Before("within(com.jar.domain.conversion.ConverterImpl)")
	void logConverterInputs(JoinPoint joinPoint) {
		logSignatureAndArgs(joinPoint);
	}
	
	private void logSignatureAndArgs(JoinPoint joinPoint) {
		LOG.debug(joinPoint.toShortString() + " with arguments: "+
		Arrays.asList(
			joinPoint.getArgs()).stream()
				.map(arg -> arg.toString())
				.collect(Collectors.joining(", "))
				);
	}
}
